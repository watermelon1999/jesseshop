package com.jessefyz.module.controller.shop;

import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.core.controller.BaseController;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.member.service.IMemberInfoService;
import com.jessefyz.module.shop.domain.LitemallComment;
import com.jessefyz.module.shop.service.ILitemallCommentService;
import com.jessefyz.module.shop.service.ILitemallGoodsService;
import com.jessefyz.module.shop.service.ILitemallTopicService;
import com.jessefyz.module.shop.service.ILitemallUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户评论服务
 */
@RestController
@RequestMapping("/wx/comment")
@Validated
public class WxCommentController extends BaseController {
    private final Log logger = LogFactory.getLog(WxCommentController.class);

    @Autowired
    private ILitemallCommentService commentService;
    @Autowired
    private ILitemallUserService userService;
    @Autowired
    private IMemberInfoService memberInfoService;
    @Autowired
    private ILitemallGoodsService goodsService;
    @Autowired
    private ILitemallTopicService topicService;

    private Object validate(LitemallComment comment) {
        String content = comment.getContent();
        if (StringUtils.isEmpty(content)) {
            return ResponseUtil.badArgument();
        }

        Integer star = comment.getStar();
        if (star == null) {
            return ResponseUtil.badArgument();
        }
        if (star < 0 || star > 5) {
            return ResponseUtil.badArgumentValue();
        }

        Integer type = comment.getType();
        Long valueId = comment.getValueId();
        if (type == null || valueId == null) {
            return ResponseUtil.badArgument();
        }
        if (type == 0) {
            if (goodsService.getById(valueId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        } else if (type == 1) {
            if (topicService.getById(valueId) == null) {
                return ResponseUtil.badArgumentValue();
            }
        } else {
            return ResponseUtil.badArgumentValue();
        }
        Integer hasPicture = comment.getHasPicture();
        if (hasPicture == null || 0 == hasPicture) {
            comment.setPicUrls(new String[0]);
        }
        return null;
    }

    /**
     * 发表评论
     *
     * @param currentMember  用户ID
     * @param comment 评论内容
     * @return 发表评论操作结果
     */
    @PostMapping("post")
    public Object post(@ApiIgnore @CurrentMember MemberInfo currentMember,
                       @RequestBody LitemallComment comment) {
        Object error = validate(comment);
        if (error != null) {
            return error;
        }
        comment.setUserId(currentMember.getId());
        commentService.save(comment);
        return ResponseUtil.ok(comment);
    }

    /**
     * 评论数量
     *
     * @param type    类型ID。 如果是0，则查询商品评论；如果是1，则查询专题评论。
     * @param valueId 商品或专题ID。如果type是0，则是商品ID；如果type是1，则是专题ID。
     * @return 评论数量
     */
    @GetMapping("count")
    public Object count(@NotNull Integer type, @NotNull Integer valueId) {
        int allCount = commentService.count(new LambdaQueryWrapper<LitemallComment>()
            .eq(LitemallComment::getValueId,valueId)
            .eq(LitemallComment::getType,type)
        );
        int hasPicCount = commentService.count(new LambdaQueryWrapper<LitemallComment>()
                .eq(LitemallComment::getValueId,valueId)
                .eq(LitemallComment::getType,type)
                .eq(LitemallComment::getHasPicture,1)
        );
        Map<String, Object> entity = new HashMap<String, Object>();
        entity.put("allCount", allCount);
        entity.put("hasPicCount", hasPicCount);
        return ResponseUtil.ok(entity);
    }

    /**
     * 评论列表
     *
     * @param type     类型ID。 如果是0，则查询商品评论；如果是1，则查询专题评论。
     * @param valueId  商品或专题ID。如果type是0，则是商品ID；如果type是1，则是专题ID。
     * @param showType 显示类型。如果是0，则查询全部；如果是1，则查询有图片的评论。
     * @param pageNum     分页页数
     * @param pageSize     分页大小
     * @return 评论列表
     */
    @GetMapping("list")
    public Object list(@NotNull Integer type,
                       @NotNull Integer valueId,
                       @NotNull Integer showType,
                       @RequestParam(defaultValue = "1") Integer pageNum,
                       @RequestParam(defaultValue = "10") Integer pageSize) {
        startPage();
        List<LitemallComment> commentList = commentService.list(new LambdaQueryWrapper<LitemallComment>()
            .eq(LitemallComment::getType,type)
            .eq(LitemallComment::getValueId,valueId)
            .eq(showType == 1,LitemallComment::getHasPicture,1)
        );
        List<Map<String, Object>> commentVoList = new ArrayList<>(commentList.size());
        for (LitemallComment comment : commentList) {
            Map<String, Object> commentVo = new HashMap<>();
            commentVo.put("addTime", DateUtils.parseDateToStr(DateUtils.YYYY_MM_DD_HH_MM_SS,comment.getAddTime()));
            commentVo.put("content", comment.getContent());
            commentVo.put("adminContent", comment.getAdminContent());
            commentVo.put("picList", comment.getPicUrls());
            commentVo.put("star", comment.getStar());
            MemberInfo userInfo = memberInfoService.getById(comment.getUserId());
            commentVo.put("userInfo", userInfo);
            commentVoList.add(commentVo);
        }
        return ResponseUtil.okList(commentVoList, commentList);
    }
}
