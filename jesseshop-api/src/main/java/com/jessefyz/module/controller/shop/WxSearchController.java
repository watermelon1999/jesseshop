package com.jessefyz.module.controller.shop;

import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.shop.domain.LitemallKeyword;
import com.jessefyz.module.shop.domain.LitemallSearchHistory;
import com.jessefyz.module.shop.service.ILitemallKeywordService;
import com.jessefyz.module.shop.service.ILitemallSearchHistoryService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 商品搜索服务
 * <p>
 * 注意：目前搜索功能非常简单，只是基于关键字匹配。
 */
@RestController
@RequestMapping("/wx/search")
@Validated
public class WxSearchController {
    private final Log logger = LogFactory.getLog(WxSearchController.class);

    @Autowired
    private ILitemallKeywordService keywordsService;
    @Autowired
    private ILitemallSearchHistoryService searchHistoryService;

    /**
     * 搜索页面信息
     * <p>
     * 如果用户已登录，则给出用户历史搜索记录；
     * 如果没有登录，则给出空历史搜索记录。
     * @return 搜索页面信息
     */
    @GetMapping("index")
    public Object index(@ApiIgnore @CurrentMember MemberInfo currentMember) {
        //取出输入框默认的关键词
        LitemallKeyword defaultKeyword = keywordsService.queryDefault();
        //取出热闹关键词
        List<LitemallKeyword> hotKeywordList = keywordsService.queryHots();

        List<LitemallSearchHistory> historyList = null;
        if (currentMember != null) {
            //取出用户历史关键字
            historyList = searchHistoryService.queryByUid(currentMember.getId());
        } else {
            historyList = new ArrayList<>(0);
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("defaultKeyword", defaultKeyword);
        data.put("historyKeywordList", historyList);
        data.put("hotKeywordList", hotKeywordList);
        return ResponseUtil.ok(data);
    }

    /**
     * 关键字提醒
     * <p>
     * 当用户输入关键字一部分时，可以推荐系统中合适的关键字。
     *
     * @param keyword 关键字
     * @return 合适的关键字
     */
    @GetMapping("helper")
    public Object helper(@NotEmpty String keyword,
                         @RequestParam(defaultValue = "1") Integer page,
                         @RequestParam(defaultValue = "10") Integer limit) {
        List<LitemallKeyword> keywordsList = keywordsService.queryByKeyword(keyword, page, limit);
        String[] keys = new String[keywordsList.size()];
        int index = 0;
        for (LitemallKeyword key : keywordsList) {
            keys[index++] = key.getKeyword();
        }
        return ResponseUtil.ok(keys);
    }

    /**
     * 清除用户搜索历史
     *
     * @return 清理是否成功
     */
    @PostMapping("clearhistory")
    public Object clearhistory(@ApiIgnore @CurrentMember MemberInfo currentMember) {
        if (currentMember == null) {
            return ResponseUtil.unlogin();
        }

        searchHistoryService.deleteByUid(currentMember.getId());
        return ResponseUtil.ok();
    }
}
