package com.jessefyz.module.controller.shop;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.core.controller.BaseController;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.common.core.page.TableDataInfo;
import com.jessefyz.common.exception.DataNotFoundException;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.shop.domain.LitemallBrand;
import com.jessefyz.module.shop.domain.req.BrandReq;
import com.jessefyz.module.shop.service.ILitemallBrandService;
import com.jessefyz.module.shop.service.ILitemallOrderGoodsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 专题服务
 */
@RestController
@RequestMapping("/wx/brand")
@Validated
public class WxBrandController extends BaseController {
    private final Log logger = LogFactory.getLog(WxBrandController.class);

    @Autowired
    private ILitemallBrandService brandService;

    @Autowired
    private ILitemallOrderGoodsService orderGoodsService;

    /**
     * 品牌列表
     * @return 品牌列表
     */
    @GetMapping("list")
    public TableDataInfo<LitemallBrand> list(@ApiIgnore @CurrentMember MemberInfo currentMember,
                                             BrandReq brandReq){
        startPage();
        QueryWrapper<LitemallBrand> lqw = new QueryWrapper<LitemallBrand>();
        brandReq.generatorQuery(lqw,false);
        List<LitemallBrand> list = brandService.list(lqw);
        return getDataTable(list);
    }

    /**
     * 品牌详情
     *
     * @param id 品牌ID
     * @return 品牌详情
     */
    @GetMapping("detail")
    public AjaxResult<LitemallBrand> detail(@NotNull Long id) {
        LitemallBrand entity = brandService.getById(id);
        if (entity == null) {
            throw new DataNotFoundException("数据不存在");
        }
        return AjaxResult.success(entity);
    }
}
