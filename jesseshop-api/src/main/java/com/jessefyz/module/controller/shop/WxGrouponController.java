package com.jessefyz.module.controller.shop;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.utils.GrouponConstant;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.member.service.IMemberInfoService;
import com.jessefyz.module.service.WxGrouponRuleService;
import com.jessefyz.module.shop.common.utils.OrderUtil;
import com.jessefyz.module.shop.constants.WxResponseCode;
import com.jessefyz.module.shop.domain.*;
import com.jessefyz.module.shop.express.ExpressService;
import com.jessefyz.module.shop.express.dao.ExpressInfo;
import com.jessefyz.module.shop.service.*;
import com.jessefyz.module.vo.GrouponRuleVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 团购服务
 * <p>
 * 需要注意这里团购规则和团购活动的关系和区别。
 */
@RestController
@RequestMapping("/wx/groupon")
@Validated
public class WxGrouponController {
    private final Log logger = LogFactory.getLog(WxGrouponController.class);

    @Autowired
    private ILitemallGrouponRulesService rulesService;
    @Autowired
    private WxGrouponRuleService wxGrouponRuleService;
    @Autowired
    private ILitemallGrouponService grouponService;
    @Autowired
    private ILitemallGoodsService goodsService;
    @Autowired
    private ILitemallOrderService orderService;
    @Autowired
    private ILitemallOrderGoodsService orderGoodsService;
    @Autowired
    private IMemberInfoService memberInfoService;
    @Autowired
    private ExpressService expressService;
    @Autowired
    private ILitemallGrouponRulesService grouponRulesService;

    /**
     * 团购规则列表
     *
     * @param page 分页页数
     * @param limit 分页大小
     * @return 团购规则列表
     */
    @GetMapping("list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "add_time") String sort,
                       @RequestParam(defaultValue = "desc") String order) {
        List<GrouponRuleVo> grouponRuleVoList = wxGrouponRuleService.queryList(page, limit, sort, order);
        return ResponseUtil.okList(grouponRuleVoList);
    }

    /**
     * 团购活动详情
     *
     * @param grouponId 团购活动ID
     * @return 团购活动详情
     */
    @GetMapping("detail")
    public Object detail(@ApiIgnore @CurrentMember MemberInfo currentMember,
                         @NotNull Integer grouponId) {

        LitemallGroupon groupon = grouponService.getOne(new LambdaQueryWrapper<LitemallGroupon>()
            .eq(LitemallGroupon::getId,grouponId)
            .eq(LitemallGroupon::getUserId,currentMember.getId())
        );

        if (groupon == null) {
            return ResponseUtil.badArgumentValue();
        }

        LitemallGrouponRules rules = rulesService.getById(groupon.getRulesId());
        if (rules == null) {
            return ResponseUtil.badArgumentValue();
        }

        // 订单信息
        LitemallOrder order = orderService.getOne(new LambdaQueryWrapper<LitemallOrder>()
                .eq(LitemallOrder::getId,groupon.getOrderId())
                .eq(LitemallOrder::getUserId,currentMember.getId())
                ,false);
        if (null == order) {
            return ResponseUtil.fail(WxResponseCode.ORDER_UNKNOWN, "订单不存在");
        }
        if (order.getUserId().longValue() != currentMember.getId().longValue()) {
            return ResponseUtil.fail(WxResponseCode.ORDER_INVALID, "不是当前用户的订单");
        }
        Map<String, Object> orderVo = new HashMap<String, Object>();
        orderVo.put("id", order.getId());
        orderVo.put("orderSn", order.getOrderSn());
        orderVo.put("addTime", order.getAddTime());
        orderVo.put("consignee", order.getConsignee());
        orderVo.put("mobile", order.getMobile());
        orderVo.put("address", order.getAddress());
        orderVo.put("goodsPrice", order.getGoodsPrice());
        orderVo.put("freightPrice", order.getFreightPrice());
        orderVo.put("actualPrice", order.getActualPrice());
        orderVo.put("orderStatusText", OrderUtil.orderStatusText(order));
        orderVo.put("handleOption", OrderUtil.build(order));
        orderVo.put("expCode", order.getShipChannel());
        orderVo.put("expNo", order.getShipSn());

        List<LitemallOrderGoods> orderGoodsList = orderGoodsService.list(new LambdaQueryWrapper<LitemallOrderGoods>()
            .eq(LitemallOrderGoods::getOrderId,order.getId())
        );
        List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
        for (LitemallOrderGoods orderGoods : orderGoodsList) {
            Map<String, Object> orderGoodsVo = new HashMap<>();
            orderGoodsVo.put("id", orderGoods.getId());
            orderGoodsVo.put("orderId", orderGoods.getOrderId());
            orderGoodsVo.put("goodsId", orderGoods.getGoodsId());
            orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
            orderGoodsVo.put("number", orderGoods.getNumber());
            orderGoodsVo.put("retailPrice", orderGoods.getPrice());
            orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
            orderGoodsVo.put("goodsSpecificationValues", orderGoods.getSpecifications());
            orderGoodsVoList.add(orderGoodsVo);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("orderInfo", orderVo);
        result.put("orderGoods", orderGoodsVoList);

        // 订单状态为已发货且物流信息不为空
        //"YTO", "800669400640887922"
        if (order.getOrderStatus().equals(OrderUtil.STATUS_SHIP)) {
            ExpressInfo ei = expressService.getExpressInfo(order.getShipChannel(), order.getShipSn());
            result.put("expressInfo", ei);
        }

        MemberInfo creator = memberInfoService.getById(groupon.getCreatorUserId());
        List<MemberInfo> joiners = new ArrayList<>();
        joiners.add(creator);
        Long linkGrouponId;
        // 这是一个团购发起记录
        if (groupon.getGrouponId() == 0) {
            linkGrouponId = groupon.getId();
        } else {
            linkGrouponId = groupon.getGrouponId();

        }
        List<LitemallGroupon> groupons = grouponService.list(new LambdaQueryWrapper<LitemallGroupon>()
            .eq(LitemallGroupon::getGrouponId,linkGrouponId)
            .eq(LitemallGroupon::getStatus, GrouponConstant.STATUS_NONE)
        );
        MemberInfo joiner;
        for (LitemallGroupon grouponItem : groupons) {
            joiner = memberInfoService.getById(grouponItem.getUserId());
            joiners.add(joiner);
        }
        result.put("linkGrouponId", linkGrouponId);
        result.put("creator", creator);
        result.put("joiners", joiners);
        result.put("groupon", groupon);
        result.put("rules", rules);
        return ResponseUtil.ok(result);
    }

    /**
     * 参加团购
     *
     * @param grouponId 团购活动ID
     * @return 操作结果
     */
    @GetMapping("join")
    public Object join(@NotNull Integer grouponId) {
        LitemallGroupon groupon = grouponService.getById(grouponId);
        if (groupon == null) {
            return ResponseUtil.badArgumentValue();
        }

        LitemallGrouponRules rules = rulesService.getById(groupon.getRulesId());
        if (rules == null) {
            return ResponseUtil.badArgumentValue();
        }

        LitemallGoods goods = goodsService.getById(rules.getGoodsId());
        if (goods == null) {
            return ResponseUtil.badArgumentValue();
        }
        Map<String, Object> result = new HashMap<>();
        result.put("groupon", groupon);
        result.put("goods", goods);

        return ResponseUtil.ok(result);
    }

    /**
     * 用户开团或入团情况
     *
     * @param showType 显示类型，如果是0，则是当前用户开的团购；否则，则是当前用户参加的团购
     * @return 用户开团或入团情况
     */
    @GetMapping("my")
    public Object my(@ApiIgnore @CurrentMember MemberInfo currentMember,
                     @RequestParam(defaultValue = "0") Integer showType) {
        List<LitemallGroupon> myGroupons = this.grouponService.list(new LambdaQueryWrapper<LitemallGroupon>()
            .eq(LitemallGroupon::getUserId,currentMember.getId())
            .eq(showType == 0,LitemallGroupon::getStatus,0)
            .ne(showType == 1,LitemallGroupon::getStatus,0)
        );
        List<Map<String, Object>> grouponVoList = new ArrayList<>(myGroupons.size());

        LitemallOrder order;
        LitemallGrouponRules rules;
        MemberInfo creator;
        for (LitemallGroupon groupon : myGroupons) {
            order = orderService.getOne(new LambdaQueryWrapper<LitemallOrder>()
                .eq(LitemallOrder::getId,groupon.getOrderId())
                .eq(LitemallOrder::getUserId,currentMember.getId())
            );
            rules = rulesService.getById(groupon.getRulesId());
            creator = memberInfoService.getById(groupon.getCreatorUserId());

            Map<String, Object> grouponVo = new HashMap<>();
            //填充团购信息
            grouponVo.put("id", groupon.getId());
            grouponVo.put("groupon", groupon);
            grouponVo.put("rules", rules);
            grouponVo.put("creator", creator.getNickname());

            Long linkGrouponId;
            // 这是一个团购发起记录
            if (groupon.getGrouponId() == 0) {
                linkGrouponId = groupon.getId();
                grouponVo.put("isCreator", creator.getId().longValue() == currentMember.getId().longValue());
            } else {
                linkGrouponId = groupon.getGrouponId();
                grouponVo.put("isCreator", false);
            }
            int joinerCount = grouponService.count(new LambdaQueryWrapper<LitemallGroupon>()
                .eq(LitemallGroupon::getGrouponId,linkGrouponId)
                .ne(LitemallGroupon::getStatus,GrouponConstant.STATUS_NONE)
            );
            grouponVo.put("joinerCount", joinerCount + 1);

            //填充订单信息
            grouponVo.put("orderId", order.getId());
            grouponVo.put("orderSn", order.getOrderSn());
            grouponVo.put("actualPrice", order.getActualPrice());
            grouponVo.put("orderStatusText", OrderUtil.orderStatusText(order));

            List<LitemallOrderGoods> orderGoodsList = orderGoodsService.list(new LambdaQueryWrapper<LitemallOrderGoods>()
                .eq(LitemallOrderGoods::getOrderId,order.getId())
            );

            List<Map<String, Object>> orderGoodsVoList = new ArrayList<>(orderGoodsList.size());
            for (LitemallOrderGoods orderGoods : orderGoodsList) {
                Map<String, Object> orderGoodsVo = new HashMap<>();
                orderGoodsVo.put("id", orderGoods.getId());
                orderGoodsVo.put("goodsName", orderGoods.getGoodsName());
                orderGoodsVo.put("number", orderGoods.getNumber());
                orderGoodsVo.put("picUrl", orderGoods.getPicUrl());
                orderGoodsVoList.add(orderGoodsVo);
            }
            grouponVo.put("goodsList", orderGoodsVoList);
            grouponVoList.add(grouponVo);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("total", grouponVoList.size());
        result.put("list", grouponVoList);

        return ResponseUtil.ok(result);
    }

}
