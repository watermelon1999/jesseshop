package com.jessefyz.module.controller.shop;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.core.controller.BaseController;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.service.HomeCacheManager;
import com.jessefyz.module.service.WxGrouponRuleService;
import com.jessefyz.module.shop.constants.SystemConfig;
import com.jessefyz.module.shop.domain.*;
import com.jessefyz.module.shop.service.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

/**
 * 首页服务
 */
@RestController
@RequestMapping("/wx/home")
@Validated
public class WxHomeController extends BaseController {
    private final Log logger = LogFactory.getLog(WxHomeController.class);

    @Autowired
    private ILitemallAdService adService;

    @Autowired
    private ILitemallGoodsService goodsService;

    @Autowired
    private ILitemallBrandService brandService;

    @Autowired
    private ILitemallTopicService topicService;

    @Autowired
    private ILitemallCategoryService categoryService;

    @Autowired
    private WxGrouponRuleService grouponService;

    @Autowired
    private ILitemallCouponService couponService;

    private final static ArrayBlockingQueue<Runnable> WORK_QUEUE = new ArrayBlockingQueue<>(9);

    private final static RejectedExecutionHandler HANDLER = new ThreadPoolExecutor.CallerRunsPolicy();

    private static ThreadPoolExecutor executorService = new ThreadPoolExecutor(9, 9, 1000, TimeUnit.MILLISECONDS, WORK_QUEUE, HANDLER);

    @GetMapping("/cache")
    public Object cache(@NotNull String key) {
        if (!key.equals("litemall_cache")) {
            return ResponseUtil.fail();
        }

        // 清除缓存
        HomeCacheManager.clearAll();
        return ResponseUtil.ok("缓存已清除");
    }

    /**
     * 首页数据
     * @param currentMember 当用户已经登录时，非空。为登录状态为null
     * @return 首页数据
     */
    @GetMapping("/index")
    public Object index(@ApiIgnore @CurrentMember MemberInfo currentMember) {
        //优先从缓存中读取
        if (HomeCacheManager.hasData(HomeCacheManager.INDEX)) {
            return ResponseUtil.ok(HomeCacheManager.getCacheData(HomeCacheManager.INDEX));
        }
        //相当于每次都是new的线程池 没意义
        //ExecutorService executorService = Executors.newFixedThreadPool(10);

        Callable<List> bannerListCallable = () -> adService.list(new LambdaQueryWrapper<LitemallAd>()
            .eq(LitemallAd::getPosition,1)
            .eq(LitemallAd::getEnabled,1)
        );

        Callable<List> channelListCallable = () -> categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
            .eq(LitemallCategory::getLevel,"L1")
        );

        Callable<List> couponListCallable;
        if(currentMember == null){
            couponListCallable = () -> couponService.list(new LambdaQueryWrapper<LitemallCoupon>()
                .orderByDesc(BaseShopEntity::getAddTime)
                .last("limit 3")
            );
        } else {
            //过滤掉用户已经领取过的
            couponListCallable = () -> couponService.list(new LambdaQueryWrapper<LitemallCoupon>()
                    .orderByDesc(BaseShopEntity::getAddTime)
                    .last("limit 3")
            );
        }

        Callable<List> newGoodsListCallable = () -> goodsService.list(new LambdaQueryWrapper<LitemallGoods>()
            .eq(LitemallGoods::getIsNew,1)
            .eq(LitemallGoods::getIsOnSale,1)
            .orderByDesc(LitemallGoods::getAddTime)
            .last("limit " + SystemConfig.getNewLimit())
        );
        Callable<List> hotGoodsListCallable = () -> goodsService.list(new LambdaQueryWrapper<LitemallGoods>()
                .eq(LitemallGoods::getIsOnSale,1)
                .orderByDesc(LitemallGoods::getAddTime)
                .last("limit " + SystemConfig.getNewLimit())
        );

        Callable<List> brandListCallable = () -> brandService.list(new LambdaQueryWrapper<LitemallBrand>()
            .last("limit "+ SystemConfig.getBrandLimit())
        );
        Callable<List> topicListCallable = () -> topicService.list(new LambdaQueryWrapper<LitemallTopic>()
                .last("limit "+ SystemConfig.getBrandLimit())
        );
        //团购专区
        Callable<List> grouponListCallable = () -> grouponService.queryList(0, 5);

        Callable<List> floorGoodsListCallable = this::getCategoryList;

        FutureTask<List> bannerTask = new FutureTask<>(bannerListCallable);
        FutureTask<List> channelTask = new FutureTask<>(channelListCallable);
        FutureTask<List> couponListTask = new FutureTask<>(couponListCallable);
        FutureTask<List> newGoodsListTask = new FutureTask<>(newGoodsListCallable);
        FutureTask<List> hotGoodsListTask = new FutureTask<>(hotGoodsListCallable);
        FutureTask<List> brandListTask = new FutureTask<>(brandListCallable);
        FutureTask<List> topicListTask = new FutureTask<>(topicListCallable);
        FutureTask<List> grouponListTask = new FutureTask<>(grouponListCallable);
        FutureTask<List> floorGoodsListTask = new FutureTask<>(floorGoodsListCallable);

        executorService.submit(bannerTask);
        executorService.submit(channelTask);
        executorService.submit(couponListTask);
        executorService.submit(newGoodsListTask);
        executorService.submit(hotGoodsListTask);
        executorService.submit(brandListTask);
        executorService.submit(topicListTask);
        executorService.submit(grouponListTask);
        executorService.submit(floorGoodsListTask);

        Map<String, Object> entity = new HashMap<>();
        try {
            entity.put("banner", bannerTask.get());
            entity.put("channel", channelTask.get());
            entity.put("couponList", couponListTask.get());
            entity.put("newGoodsList", newGoodsListTask.get());
            entity.put("hotGoodsList", hotGoodsListTask.get());
            entity.put("brandList", brandListTask.get());
            entity.put("topicList", topicListTask.get());
            entity.put("grouponList", grouponListTask.get());
            entity.put("floorGoodsList", floorGoodsListTask.get());
            //缓存数据
            HomeCacheManager.loadData(HomeCacheManager.INDEX, entity);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
//        finally {
//            executorService.shutdown();
//        }
        return ResponseUtil.ok(entity);
    }

    private List<Map> getCategoryList() {
        List<Map> categoryList = new ArrayList<>();
        List<LitemallCategory> catL1List = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
            .eq(LitemallCategory::getLevel,"L1")
            .ne(LitemallCategory::getName,"推荐")
            .orderByAsc(LitemallCategory::getSortOrder)
            .last("limit " + SystemConfig.getCatlogListLimit())
        );
        for (LitemallCategory catL1 : catL1List) {
            List<LitemallCategory> catL2List = categoryService.list(new LambdaQueryWrapper<LitemallCategory>()
                .eq(LitemallCategory::getParentId,catL1.getId())
                .orderByAsc(LitemallCategory::getSortOrder)
            );
            List<Long> l2List = new ArrayList<>();
            for (LitemallCategory catL2 : catL2List) {
                l2List.add(catL2.getId());
            }

            List<LitemallGoods> categoryGoods;
            if (l2List.size() == 0) {
                categoryGoods = new ArrayList<>();
            } else {
                categoryGoods = goodsService.list(new LambdaQueryWrapper<LitemallGoods>()
                    .eq(LitemallGoods::getCategoryId,l2List)
                    .orderByDesc(LitemallGoods::getAddTime)
                    .last("limit " + SystemConfig.getCatlogMoreLimit())
                );
            }

            Map<String, Object> catGoods = new HashMap<>();
            catGoods.put("id", catL1.getId());
            catGoods.put("name", catL1.getName());
            catGoods.put("goodsList", categoryGoods);
            categoryList.add(catGoods);
        }
        return categoryList;
    }

    /**
     * 商城介绍信息
     * @return 商城介绍信息
     */
    @GetMapping("/about")
    public Object about() {
        Map<String, Object> about = new HashMap<>();
        about.put("name", SystemConfig.getMallName());
        about.put("address", SystemConfig.getMallAddress());
        about.put("phone", SystemConfig.getMallPhone());
        about.put("qq", SystemConfig.getMallQQ());
        about.put("longitude", SystemConfig.getMallLongitude());
        about.put("latitude", SystemConfig.getMallLatitude());
        return ResponseUtil.ok(about);
    }
}
