package com.jessefyz.module.controller.shop;

import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.shop.domain.LitemallFeedback;
import com.jessefyz.module.shop.service.ILitemallFeedbackService;
import com.jessefyz.module.shop.service.ILitemallUserService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 意见反馈服务
 *
 * @author Yogeek
 * @date 2018/8/25 14:10
 */
@RestController
@RequestMapping("/wx/feedback")
@Validated
public class WxFeedbackController {
    private final Log logger = LogFactory.getLog(WxFeedbackController.class);

    @Autowired
    private ILitemallFeedbackService feedbackService;
    @Autowired
    private ILitemallUserService userService;

    private Object validate(LitemallFeedback feedback) {
        String content = feedback.getContent();
        if (StringUtils.isEmpty(content)) {
            return ResponseUtil.badArgument();
        }

        String type = feedback.getFeedType();
        if (StringUtils.isEmpty(type)) {
            return ResponseUtil.badArgument();
        }

        Integer hasPicture = feedback.getHasPicture();
        if (hasPicture == null || 0 == hasPicture) {
            feedback.setPicUrls(new String[0]);
        }

        // 测试手机号码是否正确
        String mobile = feedback.getMobile();
        if (StringUtils.isEmpty(mobile)) {
            return ResponseUtil.badArgument();
        }
//        if (!RegexUtil.isMobileSimple(mobile)) {
//            return ResponseUtil.badArgument();
//        }
        return null;
    }

    /**
     * 添加意见反馈
     *
     * @param feedback 意见反馈
     * @return 操作结果
     */
    @PostMapping("submit")
    public Object submit(@ApiIgnore @CurrentMember MemberInfo currentMember,
                         @RequestBody LitemallFeedback feedback) {
        Object error = validate(feedback);
        if (error != null) {
            return error;
        }

        String username = currentMember.getUsername();
        feedback.setId(null);
        feedback.setUserId(currentMember.getId());
        feedback.setUsername(username);
        //状态默认是0，1表示状态已发生变化
        feedback.setStatus(1);
        feedbackService.save(feedback);

        return ResponseUtil.ok();
    }

}
