package com.jessefyz.module.controller.shop;

import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.service.GetRegionService;
import com.jessefyz.module.shop.domain.LitemallAddress;
import com.jessefyz.module.shop.service.ILitemallAddressService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.List;

/**
 * 用户收货地址服务
 */
@Api(tags = "收货地址")
@RestController
@RequestMapping("/wx/address")
@Validated
public class WxAddressController extends GetRegionService {
	private final Log logger = LogFactory.getLog(WxAddressController.class);

	@Autowired
	private ILitemallAddressService addressService;

	/**
	 * 用户收货地址列表
	 *
	 * @return 收货地址列表
	 */
	@ApiOperation("用户收货地址列表")
	@GetMapping("list")
	public Object list(@ApiIgnore @CurrentMember MemberInfo currentMember) {
		List<LitemallAddress> addressList = addressService.selectLitemallAddressList(LitemallAddress.builder()
			.userId(currentMember.getId())
			.build()
		);
		return ResponseUtil.okList(addressList);
	}

	/**
	 * 收货地址详情
	 * @param id:收货地址ID
	 * @return 收货地址详情
	 */
	@ApiOperation("收货地址详情")
	@GetMapping("/detail/{id}")
	public AjaxResult<LitemallAddress> detail(@ApiIgnore @CurrentMember MemberInfo currentMember, @PathVariable Long id) {
		return AjaxResult.success(this.addressService.getDetailFromWx(currentMember.getId(),id));
	}

	/**
	 * 添加或更新收货地址
	 * @param address 用户收货地址
	 * @return 添加或更新操作结果
	 */
	@ApiOperation("添加")
	@PostMapping("save")
	public AjaxResult<Long> save(@ApiIgnore @CurrentMember MemberInfo currentMember,@Valid @RequestBody LitemallAddress address) {
		address.setUserId(currentMember.getId());
		return AjaxResult.success(this.addressService.saveOrUpdateAddress(address));
	}

	/**
	 * 删除收货地址
	 * @return 删除操作结果
	 */
	@ApiOperation("删除")
	@PostMapping("/delete/{id}")
	public AjaxResult delete(@ApiIgnore @CurrentMember MemberInfo currentMember, @PathVariable Long id) {
		this.addressService.getDetailFromWx(currentMember.getId(),id);
		addressService.removeById(id);
		return AjaxResult.success("删除成功");
	}
}
