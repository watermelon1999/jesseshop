package com.jessefyz.module.controller.shop;

import cn.binarywang.wx.miniapp.api.WxMaService;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.core.domain.AjaxResult;
import com.jessefyz.common.exception.DataNotFoundException;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.dto.*;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.service.IApiLoginService;
import com.jessefyz.module.vo.MemberVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


/**
 * 鉴权服务
 */
@RestController
@RequestMapping("/wx/auth")
@Validated
public class WxAuthController {
    private final Log logger = LogFactory.getLog(WxAuthController.class);

    @Autowired
    private WxMaService wxService;

    @Autowired
    private IApiLoginService apiLoginService;

    /**
     * 账号登录
     * @param request 请求对象
     * @return 登录结果
     */
    @PostMapping("login")
    public AjaxResult<MemberVo> loginByAccount(@Valid  @RequestBody AccountLoginInfo accountLoginInfo, HttpServletRequest request) {
        return this.apiLoginService.loginByAccount(accountLoginInfo,request);
    }

    /**
     * 微信小程序登录
     * @return 登录结果
     */
    @PostMapping("login_by_weixin")
    public AjaxResult<MemberVo> loginByWeixin(@RequestBody WxLoginInfo wxLoginInfo, HttpServletRequest request) throws Exception {
       return  apiLoginService.loginBywechatMiniApp(wxLoginInfo,request);
    }


    /**
     * 请求注册验证码
     *
     * TODO
     * 这里需要一定机制防止短信验证码被滥用
     *
     * @return
     */
    @PostMapping("regCaptcha")
    public AjaxResult registerCaptcha(@Valid @RequestBody RegisterCaptcha registerCaptcha) {
        apiLoginService.registerCaptcha(registerCaptcha);
        return AjaxResult.success();
    }

    /**
     * 账号注册
     *
     *   请求内容
     *                {
     *                username: xxx,
     *                password: xxx,
     *                mobile: xxx
     *                code: xxx
     *                }
     *                其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果
     * 成功则
     * {
     * errno: 0,
     * errmsg: '成功',
     * data:
     * {
     * token: xxx,
     * tokenExpire: xxx,
     * userInfo: xxx
     * }
     * }
     * 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("register")
    public AjaxResult<MemberVo> register(@Valid @RequestBody Register register, HttpServletRequest request) {
        return apiLoginService.register(register,request);
    }

    /**
     * 请求验证码
     *
     * TODO
     * 这里需要一定机制防止短信验证码被滥用
     *
     * @param  { mobile: xxx, type: xxx }
     * @return
     */
    @PostMapping("captcha")
    public AjaxResult captcha(@ApiIgnore @CurrentMember MemberInfo currentMember,@Valid @RequestBody Captcha captcha) {
        this.apiLoginService.captcha(captcha);
        return AjaxResult.success();
    }

    /**
     * 账号密码重置
     *
     * @param
     *                {
     *                password: xxx,
     *                mobile: xxx
     *                code: xxx
     *                }
     *                其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果
     * 成功则 { errno: 0, errmsg: '成功' }
     * 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("reset")
    public AjaxResult reset(
                        @Valid @RequestBody ResetPassword resetPassword,
                        HttpServletRequest request) {
        this.apiLoginService.reset(resetPassword);
        return AjaxResult.success();
    }

    /**
     * 账号手机号码重置
     *
     * @param
     *                {
     *                password: xxx,
     *                mobile: xxx
     *                code: xxx
     *                }
     *                其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果
     * 成功则 { errno: 0, errmsg: '成功' }
     * 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("resetPhone")
    public AjaxResult resetPhone(@ApiIgnore @CurrentMember MemberInfo currentMember,
                             @Valid @RequestBody ResetPassword resetPassword, HttpServletRequest request) {

        this.apiLoginService.resetPhone(currentMember,resetPassword);
        return AjaxResult.success();
    }

    /**
     * 账号信息更新
     *
     * @param
     *                {
     *                password: xxx,
     *                mobile: xxx
     *                code: xxx
     *                }
     *                其中code是手机验证码，目前还不支持手机短信验证码
     * @param request 请求对象
     * @return 登录结果
     * 成功则 { errno: 0, errmsg: '成功' }
     * 失败则 { errno: XXX, errmsg: XXX }
     */
    @PostMapping("profile")
    public AjaxResult profile(@ApiIgnore @CurrentMember MemberInfo currentMember,
                          @Valid @RequestBody ProfileUpdate profileUpdate,
                          HttpServletRequest request) {
        this.apiLoginService.profile(currentMember,profileUpdate);
        return AjaxResult.success();
    }

    /**
     * 微信手机号码绑定
     *
     * @param
     * @param
     * @return
     */
    @PostMapping("bindPhone")
    public AjaxResult bindPhone(@ApiIgnore @CurrentMember MemberInfo currentMember,
                            @Valid @RequestBody WxBindPhone wxBindPhone
                            ) {
        this.apiLoginService.bindPhone(currentMember,wxBindPhone);
        return AjaxResult.success();
    }

    @PostMapping("logMemberout")
    public Object logout(@ApiIgnore @CurrentMember MemberInfo currentMember) {
        return ResponseUtil.ok();
    }

    @GetMapping("info")
    public AjaxResult<MemberVo> info(@ApiIgnore @CurrentMember MemberInfo currentMember) {
        if (null == currentMember) {
            throw  new DataNotFoundException("会员不存在");
        }
        MemberVo memberVo = apiLoginService.getMemberVo(currentMember);
        return AjaxResult.success(memberVo);
    }
}
