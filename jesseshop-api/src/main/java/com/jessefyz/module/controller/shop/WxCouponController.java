package com.jessefyz.module.controller.shop;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.jessefyz.aspectj.CurrentMember;
import com.jessefyz.common.core.controller.BaseController;
import com.jessefyz.common.exception.DataNotFoundException;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.common.utils.JacksonUtil;
import com.jessefyz.common.utils.ResponseUtil;
import com.jessefyz.module.member.domain.MemberInfo;
import com.jessefyz.module.service.CouponVerifyService;
import com.jessefyz.module.shop.constants.CouponConstant;
import com.jessefyz.module.shop.constants.CouponUserConstant;
import com.jessefyz.module.shop.constants.WxResponseCode;
import com.jessefyz.module.shop.domain.LitemallCart;
import com.jessefyz.module.shop.domain.LitemallCoupon;
import com.jessefyz.module.shop.domain.LitemallCouponUser;
import com.jessefyz.module.shop.domain.LitemallGrouponRules;
import com.jessefyz.module.shop.domain.vo.CouponVo;
import com.jessefyz.module.shop.service.ILitemallCartService;
import com.jessefyz.module.shop.service.ILitemallCouponService;
import com.jessefyz.module.shop.service.ILitemallCouponUserService;
import com.jessefyz.module.shop.service.ILitemallGrouponRulesService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 优惠券服务
 */
@RestController
@RequestMapping("/wx/coupon")
@Validated
public class WxCouponController extends BaseController {
    private final Log logger = LogFactory.getLog(WxCouponController.class);

    @Autowired
    private ILitemallCouponService couponService;
    @Autowired
    private ILitemallCouponUserService couponUserService;
    @Autowired
    private ILitemallGrouponRulesService grouponRulesService;
    @Autowired
    private ILitemallCartService cartService;
    @Autowired
    private CouponVerifyService couponVerifyService;

    /**
     * 优惠券列表
     *
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @GetMapping("list")
    public Object list(@RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @RequestParam(defaultValue = "add_time") String sort,
                       @RequestParam(defaultValue = "desc") String order) {
        startPage();
//        List<LitemallCoupon> couponList = couponService.queryList(page, limit, sort, order);
        List<LitemallCoupon> couponList = couponService.list(new LambdaQueryWrapper<LitemallCoupon>());
        return getDataTable(couponList);
    }

    /**
     * 个人优惠券列表
     *
     * @param status
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @GetMapping("mylist")
    public Object mylist(@ApiIgnore @CurrentMember MemberInfo currentMember,
                       Integer status,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit, @RequestParam(defaultValue = "add_time") String sort,
                       @RequestParam(defaultValue = "desc") String order) {
        startPage();
        List<LitemallCouponUser> couponUserList = couponUserService.list(new LambdaQueryWrapper<LitemallCouponUser>()
            .eq(LitemallCouponUser::getStatus,status)
        );
//                .queryList(currentMember.getId(), null, status, page, limit, sort, order);
        List<CouponVo> couponVoList = change(couponUserList);
        return ResponseUtil.okList(couponVoList, couponUserList);
    }

    private List<CouponVo> change(List<LitemallCouponUser> couponList) {
        List<CouponVo> couponVoList = new ArrayList<>(couponList.size());
        for(LitemallCouponUser couponUser : couponList){
            Long couponId = couponUser.getCouponId();
            LitemallCoupon coupon = couponService.getById(couponId);
            couponVoList.add(CouponVo.builder()
                    .id(couponUser.getId())
                    .cid(coupon.getId())
                    .name(coupon.getName())
                    .desc(coupon.getDescription())
                    .tag(coupon.getTag())
                    .min(coupon.getMin())
                    .discount(coupon.getDiscount())
                    .startTime(couponUser.getStartTime())
                    .endTime(couponUser.getEndTime())
                    .build());
        }

        return couponVoList;
    }


    /**
     * 当前购物车下单商品订单可用优惠券
     *
     * @param userId
     * @param cartId
     * @param grouponRulesId
     * @return
     */
    @GetMapping("selectlist")
    public Object selectlist(@ApiIgnore @CurrentMember MemberInfo currentMember,
                             Long cartId, Long grouponRulesId) {

        // 团购优惠
        BigDecimal grouponPrice = new BigDecimal(0.00);
        LitemallGrouponRules grouponRules = grouponRulesService.getById(grouponRulesId);
        if (grouponRules != null) {
            grouponPrice = grouponRules.getDiscount();
        }

        // 商品价格
        List<LitemallCart> checkedGoodsList = null;
        if (cartId == null || cartId.equals(0L)) {
            checkedGoodsList = cartService.list(new LambdaQueryWrapper<LitemallCart>()
                .eq(LitemallCart::getUserId,currentMember.getId())
                .eq(LitemallCart::getChecked,1)
            );
        } else {
            LitemallCart cart = cartService.getOne(new LambdaQueryWrapper<LitemallCart>()
                    .eq(LitemallCart::getId,cartId)
                    .eq(LitemallCart::getUserId,currentMember.getId())
                    ,false);
            if (cart == null) {
                throw new DataNotFoundException("购物车不存在");
            }
            checkedGoodsList = new ArrayList<>(1);
            checkedGoodsList.add(cart);
        }
        BigDecimal checkedGoodsPrice = new BigDecimal(0.00);
        for (LitemallCart cart : checkedGoodsList) {
            //  只有当团购规格商品ID符合才进行团购优惠
            if (grouponRules != null && grouponRules.getGoodsId().equals(cart.getGoodsId())) {
                checkedGoodsPrice = checkedGoodsPrice.add(cart.getPrice().subtract(grouponPrice).multiply(new BigDecimal(cart.getNumber())));
            } else {
                checkedGoodsPrice = checkedGoodsPrice.add(cart.getPrice().multiply(new BigDecimal(cart.getNumber())));
            }
        }
        // 计算优惠券可用情况
        List<LitemallCouponUser> couponUserList = couponUserService.list(new LambdaQueryWrapper<LitemallCouponUser>()
            .eq(LitemallCouponUser::getUserId,currentMember.getId())
            .eq(LitemallCouponUser::getStatus, CouponUserConstant.STATUS_USABLE)
        );
        List<CouponVo> couponVoList = change(couponUserList);
        for (CouponVo cv : couponVoList) {
            LitemallCoupon coupon = couponVerifyService.checkCoupon(currentMember.getId(), cv.getCid(), cv.getId(), checkedGoodsPrice, checkedGoodsList);
            cv.setAvailable(coupon != null);
        }

        return ResponseUtil.okList(couponVoList);
    }

    /**
     * 优惠券领取
     *
     * @param body 请求内容， { couponId: xxx }
     * @return 操作结果
     */
    @PostMapping("receive")
    public Object receive(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body) {

        Integer couponId = JacksonUtil.parseInteger(body, "couponId");
        if(couponId == null){
            return ResponseUtil.badArgument();
        }

        LitemallCoupon coupon = couponService.getById(couponId);
        if(coupon == null){
            return ResponseUtil.badArgumentValue();
        }

        // 当前已领取数量和总数量比较
        Integer total = coupon.getTotal();
        Integer totalCoupons = couponUserService.count(new LambdaQueryWrapper<LitemallCouponUser>()
            .eq(LitemallCouponUser::getCouponId,coupon.getId())
        );
        if((total != 0) && (totalCoupons >= total)){
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已领完");
        }

        // 当前用户已领取数量和用户限领数量比较
        Integer limit = coupon.getLimit().intValue();
        Integer userCounpons = couponUserService.count(new LambdaQueryWrapper<LitemallCouponUser>()
                .eq(LitemallCouponUser::getUserId,currentMember.getId())
                .eq(LitemallCouponUser::getCouponId,coupon.getId())
        );
        if((limit != 0) && (userCounpons >= limit)){
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已经领取过");
        }

        // 优惠券分发类型
        // 例如注册赠券类型的优惠券不能领取
        Integer type = coupon.getType();
        if(type.equals(CouponConstant.TYPE_REGISTER)){
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "新用户优惠券自动发送");
        }
        else if(type.equals(CouponConstant.TYPE_CODE)){
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券只能兑换");
        }
        else if(!type.equals(CouponConstant.TYPE_COMMON)){
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券类型不支持");
        }

        // 优惠券状态，已下架或者过期不能领取
        Integer status = coupon.getStatus();
        if(status.equals(CouponConstant.STATUS_OUT)){
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已领完");
        }
        else if(status.equals(CouponConstant.STATUS_EXPIRED)){
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券已经过期");
        }

        // 用户领券记录
        LitemallCouponUser couponUser = new LitemallCouponUser();
        couponUser.setCouponId(Long.valueOf(couponId));
        couponUser.setUserId(currentMember.getId());
        Integer timeType = coupon.getTimeType();
        if (timeType.equals(CouponConstant.TIME_TYPE_TIME)) {
            couponUser.setStartTime(coupon.getStartTime());
            couponUser.setEndTime(coupon.getEndTime());
        }
        else{
            couponUser.setStartTime(new Date());
            couponUser.setEndTime(DateUtils.addDays(couponUser.getStartTime(),coupon.getDays()));
        }
        couponUserService.save(couponUser);

        return ResponseUtil.ok();
    }

    /**
     * 优惠券兑换
     *
     * @param body 请求内容， { code: xxx }
     * @return 操作结果
     */
    @PostMapping("exchange")
    public Object exchange(@ApiIgnore @CurrentMember MemberInfo currentMember, @RequestBody String body) {
        String code = JacksonUtil.parseString(body, "code");
        if(code == null){
            return ResponseUtil.badArgument();
        }

        LitemallCoupon coupon = couponService.getOne(new LambdaQueryWrapper<LitemallCoupon>()
            .eq(LitemallCoupon::getCode,code)
            .eq(LitemallCoupon::getType,CouponConstant.TYPE_CODE)
            .eq(LitemallCoupon::getStatus,CouponConstant.STATUS_NORMAL)
                ,false);
        if(coupon == null){
            return ResponseUtil.fail(WxResponseCode.COUPON_CODE_INVALID, "优惠券不正确");
        }
        Long couponId = coupon.getId();

        // 当前已领取数量和总数量比较
        Integer total = coupon.getTotal();
        Integer totalCoupons = couponUserService.count(new LambdaQueryWrapper<LitemallCouponUser>()
                .eq(LitemallCouponUser::getCouponId,coupon.getId())
        );
        if((total != 0) && (totalCoupons >= total)){
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已兑换");
        }

        // 当前用户已领取数量和用户限领数量比较
        Integer limit = coupon.getLimit().intValue();
        Integer userCounpons = couponUserService.count(new LambdaQueryWrapper<LitemallCouponUser>()
                .eq(LitemallCouponUser::getUserId,currentMember.getId())
                .eq(LitemallCouponUser::getCouponId,coupon.getId())
        );
        if((limit != 0) && (userCounpons >= limit)){
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已兑换");
        }

        // 优惠券分发类型
        // 例如注册赠券类型的优惠券不能领取
        Integer type = coupon.getType();
        if(type.equals(CouponConstant.TYPE_REGISTER)){
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "新用户优惠券自动发送");
        }
        else if(type.equals(CouponConstant.TYPE_COMMON)){
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券只能领取，不能兑换");
        }
        else if(!type.equals(CouponConstant.TYPE_CODE)){
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券类型不支持");
        }

        // 优惠券状态，已下架或者过期不能领取
        Integer status = coupon.getStatus();
        if(status.equals(CouponConstant.STATUS_OUT)){
            return ResponseUtil.fail(WxResponseCode.COUPON_EXCEED_LIMIT, "优惠券已兑换");
        }
        else if(status.equals(CouponConstant.STATUS_EXPIRED)){
            return ResponseUtil.fail(WxResponseCode.COUPON_RECEIVE_FAIL, "优惠券已经过期");
        }

        // 用户领券记录
        LitemallCouponUser couponUser = new LitemallCouponUser();
        couponUser.setCouponId(couponId);
        couponUser.setUserId(currentMember.getId());
        Integer timeType = coupon.getTimeType();
        if (timeType.equals(CouponConstant.TIME_TYPE_TIME)) {
            couponUser.setStartTime(coupon.getStartTime());
            couponUser.setEndTime(coupon.getEndTime());
        }
        else{
            couponUser.setStartTime(new Date());
            couponUser.setEndTime(DateUtils.addDays(couponUser.getStartTime(),coupon.getDays()));
        }
        couponUserService.save(couponUser);

        return ResponseUtil.ok();
    }
}
