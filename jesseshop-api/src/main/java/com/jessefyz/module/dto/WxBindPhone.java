package com.jessefyz.module.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * 授权登录请求体
 */
@Data
@ApiModel
@Validated
public class WxBindPhone {


    @ApiModelProperty(name="iv",value = "iv-必填",notes = "iv", required = true, dataType = "String")
    @NotBlank(message = "iv不能为空")
    protected String iv;

    @ApiModelProperty(name="encryptedData",value = "encryptedData-必填",notes = "encryptedData", required = true, dataType = "String")
    @NotBlank(message = "encryptedData不能为空")
    protected String encryptedData;

}
