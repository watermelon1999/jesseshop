package com.jessefyz.module.dto;


import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Data
@Validated
public class RegisterCaptcha {
    @NotBlank(message = "手机号不能为空")
    private String mobile;

}
