package com.jessefyz.module.merchant.wx.core.kit;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.util.StrUtil;

import javax.crypto.Cipher;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.*;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>IJPay 让支付触手可及，封装了微信支付、支付宝支付、银联支付常用的支付方式以及各种常用的接口。</p>
 *
 * <p>不依赖任何第三方 mvc 框架，仅仅作为工具使用简单快速完成支付模块的开发，可轻松嵌入到任何系统里。 </p>
 *
 * <p>IJPay 交流群: 723992875</p>
 *
 * <p>Node.js 版: https://gitee.com/javen205/TNWX</p>
 *
 * <p>RSA 非对称加密工具类</p>
 *
 * @author Javen
 */
public class RsaKit {

    /**
     * RSA最大加密明文大小
     */
    private static final int MAX_ENCRYPT_BLOCK = 117;

    /**
     * RSA最大解密密文大小
     */
    private static final int MAX_DECRYPT_BLOCK = 128;

    /**
     * 加密算法RSA
     */
    private static final String KEY_ALGORITHM = "RSA";

    /**
     * 生成公钥和私钥
     *
     * @throws Exception 异常信息
     */
    public static Map<String, String> getKeys() throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
        keyPairGen.initialize(1024);
        KeyPair keyPair = keyPairGen.generateKeyPair();
        RSAPublicKey publicKey = (RSAPublicKey) keyPair.getPublic();
        RSAPrivateKey privateKey = (RSAPrivateKey) keyPair.getPrivate();

        String publicKeyStr = getPublicKeyStr(publicKey);
        String privateKeyStr = getPrivateKeyStr(privateKey);

        Map<String, String> map = new HashMap<String, String>(2);
        map.put("publicKey", publicKeyStr);
        map.put("privateKey", privateKeyStr);

        System.out.println("公钥\r\n" + publicKeyStr);
        System.out.println("私钥\r\n" + privateKeyStr);
        return map;
    }

    /**
     * 使用模和指数生成RSA公钥
     * 注意：【此代码用了默认补位方式，为RSA/None/PKCS1Padding，不同JDK默认的补位方式可能不同，如Android默认是RSA
     * /None/NoPadding】
     *
     * @param modulus  模
     * @param exponent 公钥指数
     * @return {@link RSAPublicKey}
     */
    public static RSAPublicKey getPublicKey(String modulus, String exponent) {
        try {
            BigInteger b1 = new BigInteger(modulus);
            BigInteger b2 = new BigInteger(exponent);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            RSAPublicKeySpec keySpec = new RSAPublicKeySpec(b1, b2);
            return (RSAPublicKey) keyFactory.generatePublic(keySpec);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 使用模和指数生成RSA私钥
     * 注意：【此代码用了默认补位方式，为RSA/None/PKCS1Padding，不同JDK默认的补位方式可能不同，如Android默认是RSA
     * /None/NoPadding】
     *
     * @param modulus  模
     * @param exponent 指数
     * @return {@link RSAPrivateKey}
     */
    public static RSAPrivateKey getPrivateKey(String modulus, String exponent) {
        try {
            BigInteger b1 = new BigInteger(modulus);
            BigInteger b2 = new BigInteger(exponent);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(b1, b2);
            return (RSAPrivateKey) keyFactory.generatePrivate(keySpec);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 公钥加密
     *
     * @param data      需要加密的数据
     * @param publicKey 公钥
     * @return 加密后的数据
     * @throws Exception 异常信息
     */
    public static String encryptByPublicKey(String data, String publicKey) throws Exception {
        return encryptByPublicKey(data, publicKey, "RSA/ECB/PKCS1Padding");
    }

    /**
     * 公钥加密
     *
     * @param data      需要加密的数据
     * @param publicKey 公钥
     * @return 加密后的数据
     * @throws Exception 异常信息
     */
    public static String encryptByPublicKeyByWx(String data, String publicKey) throws Exception {
        return encryptByPublicKey(data, publicKey, "RSA/ECB/OAEPWITHSHA-1ANDMGF1PADDING");
    }

    /**
     * 公钥加密
     *
     * @param data      需要加密的数据
     * @param publicKey 公钥
     * @param fillMode  填充模式
     * @return 加密后的数据
     * @throws Exception 异常信息
     */
    public static String encryptByPublicKey(String data, String publicKey, String fillMode) throws Exception {
        byte[] dataByte = data.getBytes(StandardCharsets.UTF_8);
        byte[] keyBytes = Base64.decode(publicKey);
        X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key key = keyFactory.generatePublic(x509KeySpec);
        // 对数据加密
        Cipher cipher = Cipher.getInstance(fillMode);
        cipher.init(Cipher.ENCRYPT_MODE, key);
        int inputLen = dataByte.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段加密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_ENCRYPT_BLOCK) {
                cache = cipher.doFinal(dataByte, offSet, MAX_ENCRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(dataByte, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_ENCRYPT_BLOCK;
        }
        byte[] encryptedData = out.toByteArray();
        out.close();
        return StrUtil.str(Base64.encode(encryptedData));
    }

    /**
     * 私钥签名
     *
     * @param data       需要加密的数据
     * @param privateKey 私钥
     * @return 加密后的数据
     * @throws Exception 异常信息
     */
    public static String encryptByPrivateKey(String data, String privateKey) throws Exception {
        PKCS8EncodedKeySpec priPkcs8 = new PKCS8EncodedKeySpec(Base64.decode(privateKey));
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        PrivateKey priKey = keyFactory.generatePrivate(priPkcs8);
        Signature signature = Signature.getInstance("SHA256WithRSA");

        signature.initSign(priKey);
        signature.update(data.getBytes(StandardCharsets.UTF_8));
        byte[] signed = signature.sign();
        return StrUtil.str(Base64.encode(signed));
    }

    /**
     * 私钥签名
     *
     * @param data       需要加密的数据
     * @param privateKey 私钥
     * @return 加密后的数据
     * @throws Exception 异常信息
     */
    public static String encryptByPrivateKey(String data, PrivateKey privateKey) throws Exception {
        Signature signature = Signature.getInstance("SHA256WithRSA");
        signature.initSign(privateKey);
        signature.update(data.getBytes(StandardCharsets.UTF_8));
        byte[] signed = signature.sign();
        return StrUtil.str(Base64.encode(signed));
    }

    /**
     * 公钥验证签名
     *
     * @param data      需要加密的数据
     * @param sign      签名
     * @param publicKey 公钥
     * @return 验证结果
     * @throws Exception 异常信息
     */
    public static boolean checkByPublicKey(String data, String sign, String publicKey) throws Exception {
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        byte[] encodedKey = Base64.decode(publicKey);
        PublicKey pubKey = keyFactory.generatePublic(new X509EncodedKeySpec(encodedKey));
        Signature signature = Signature.getInstance("SHA256WithRSA");
        signature.initVerify(pubKey);
        signature.update(data.getBytes(StandardCharsets.UTF_8));
        return signature.verify(Base64.decode(sign.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * 公钥验证签名
     *
     * @param data      需要加密的数据
     * @param sign      签名
     * @param publicKey 公钥
     * @return 验证结果
     * @throws Exception 异常信息
     */
    public static boolean checkByPublicKey(String data, String sign, PublicKey publicKey) throws Exception {
        Signature signature = Signature.getInstance("SHA256WithRSA");
        signature.initVerify(publicKey);
        signature.update(data.getBytes(StandardCharsets.UTF_8));
        return signature.verify(Base64.decode(sign.getBytes(StandardCharsets.UTF_8)));
    }

    /**
     * 私钥解密
     *
     * @param data       需要解密的数据
     * @param privateKey 私钥
     * @return 解密后的数据
     * @throws Exception 异常信息
     */
    public static String decryptByPrivateKey(String data, String privateKey) throws Exception {
        return decryptByPrivateKey(data, privateKey, "RSA/ECB/PKCS1Padding");
    }

    /**
     * 私钥解密
     *
     * @param data       需要解密的数据
     * @param privateKey 私钥
     * @return 解密后的数据
     * @throws Exception 异常信息
     */
    public static String decryptByPrivateKeyByWx(String data, String privateKey) throws Exception {
        return decryptByPrivateKey(data, privateKey, "RSA/ECB/OAEPWITHSHA-1ANDMGF1PADDING");
    }

    /**
     * 私钥解密
     *
     * @param data       需要解密的数据
     * @param privateKey 私钥
     * @param fillMode   填充模式
     * @return 解密后的数据
     * @throws Exception 异常信息
     */
    public static String decryptByPrivateKey(String data, String privateKey, String fillMode) throws Exception {
        byte[] encryptedData = Base64.decode(data);
        byte[] keyBytes = Base64.decode(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
        Key key = keyFactory.generatePrivate(pkcs8KeySpec);
        Cipher cipher = Cipher.getInstance(fillMode);

        cipher.init(Cipher.DECRYPT_MODE, key);
        int inputLen = encryptedData.length;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        int offSet = 0;
        byte[] cache;
        int i = 0;
        // 对数据分段解密
        while (inputLen - offSet > 0) {
            if (inputLen - offSet > MAX_DECRYPT_BLOCK) {
                cache = cipher.doFinal(encryptedData, offSet, MAX_DECRYPT_BLOCK);
            } else {
                cache = cipher.doFinal(encryptedData, offSet, inputLen - offSet);
            }
            out.write(cache, 0, cache.length);
            i++;
            offSet = i * MAX_DECRYPT_BLOCK;
        }
        byte[] decryptedData = out.toByteArray();
        out.close();
        return new String(decryptedData);
    }

    /**
     * 从字符串中加载公钥
     *
     * @param publicKeyStr 公钥数据字符串
     * @throws Exception 异常信息
     */
    public static PublicKey loadPublicKey(String publicKeyStr) throws Exception {
        try {
            byte[] buffer = Base64.decode(publicKeyStr);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(buffer);
            return keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("公钥非法");
        } catch (NullPointerException e) {
            throw new Exception("公钥数据为空");
        }
    }

    /**
     * 从字符串中加载私钥<br>
     * 加载时使用的是PKCS8EncodedKeySpec（PKCS#8编码的Key指令）。
     *
     * @param privateKeyStr 私钥
     * @return {@link PrivateKey}
     * @throws Exception 异常信息
     */
    public static PrivateKey loadPrivateKey(String privateKeyStr) throws Exception {
        try {
            byte[] buffer = Base64.decode(privateKeyStr);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(buffer);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            return keyFactory.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException e) {
            throw new Exception("私钥非法");
        } catch (NullPointerException e) {
            throw new Exception("私钥数据为空");
        }
    }

    public static String getPrivateKeyStr(PrivateKey privateKey) {
        return Base64.encode(privateKey.getEncoded());
    }

    public static String getPublicKeyStr(PublicKey publicKey) {
        return Base64.encode(publicKey.getEncoded());
    }

    public static void main(String[] args) throws Exception {
		 String privateKey1 = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMJSUlGSrod2lkMMNF8YHvTwKaD1T2JkjPhX8Mi8w2g1x25a5KcuHAVdXGJJEUJouNa4M9u9llnxOusv6/kKxi6ddNoalF8osQFKy0IgMwDC1JsqnNU+fBbkiDCJiecl0P4bml5jeaa4d39eBtLHTeoL80JYuuWCOTKmPl7D3tB9AgMBAAECgYAn/KSv4I1FluOI3IQSZXZJVPpcAt7ZEnPnzsDfrehQE3D8hnuy40wk1ndiSkLg9YfDt5gESdkcnQVQzEW0s8DgDum7ZZKc6QhlS6woek4/Zaxx+IoSUwygGOwryxcu3cbuQiQZYWPywt3oq/S2VkuRwXvGL828bCxDno4J7pi1gQJBAOjXDaHI1ZnNsuKd7x+DtWoQbhaeSLdzEZMHFISSQgcuQ08pAVYvZEJfTAfmy07sBXywgyOdDKNXA/UDUySdifECQQDVpnLsx2PTzt/tN8MHiu3gr2C67UvZ7vRusc4qpfuCrZZLKgkohiDFRwU/xnamxxt5tJK4XTT55tsnMgAbRoNNAkBziJocB/JtMdMsfV90zL7M1UA6+ulsFa3HvcChk40t0PfXJKqkIDnocS7h0NRa4eRLHa7Ekh3+QqQVBERSK9WRAkBO9QCr+NQvxtiCtoThGLroumepLWFG3USAVD25DTZyCOzuQk64JzmdLq4F77aR1CuXQuCmOewGHwcG6TeY3OpRAkEAs40xvT2iYNeLUvr3Y9h6k1vzsTFtSjLp2Voz8aWDg/by5tHArkYT/Hlfma063aP4/GLCqm5GT/h7DuPtdadu9Q==";


		String certificate ="MIID3DCCAsSgAwIBAgIUEfNFhpDCDj1lEeoogbmXe8VxU2cwDQYJKoZIhvcNAQEL" +
			"BQAwXjELMAkGA1UEBhMCQ04xEzARBgNVBAoTClRlbnBheS5jb20xHTAbBgNVBAsT" +
			"FFRlbnBheS5jb20gQ0EgQ2VudGVyMRswGQYDVQQDExJUZW5wYXkuY29tIFJvb3Qg" +
			"Q0EwHhcNMjAwMzI0MDUyNzA0WhcNMjUwMzIzMDUyNzA0WjBuMRgwFgYDVQQDDA9U" +
			"ZW5wYXkuY29tIHNpZ24xEzARBgNVBAoMClRlbnBheS5jb20xHTAbBgNVBAsMFFRl" +
			"bnBheS5jb20gQ0EgQ2VudGVyMQswCQYDVQQGDAJDTjERMA8GA1UEBwwIU2hlblpo" +
			"ZW4wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDm/4cIVZW+we92jGg7" +
			"jwYRBqqvUuChZ5dLhIez3/R9CimagkOsK8RtxFNTKi9l1aLi2iX/AKgWyXT8+VOl" +
			"w5x63PcEHpgmELpNKwZaYw1ny1Z5KR1h9WKc3xv9m6gwreMLqCfkSfHRdfch9Fph" +
			"AZI+ddNLb15cLk/TnfXXLlmjYDw1szXB+qLjNPETg/xvNjenYiX5oWEzfFkfscNT" +
			"BAyXOTTp7N81OWonIn2EPccGg0iT6HWTbmnChUaqXh5a/sbS/Td6sQAOZAkqUDQI" +
			"VvR0mubum2wx8DaTSWusiICNWuZDchy49sxuhY1jvVNgd02vzjREloPLlVcew6ZU" +
			"n+31AgMBAAGjgYEwfzAJBgNVHRMEAjAAMAsGA1UdDwQEAwIE8DBlBgNVHR8EXjBc" +
			"MFqgWKBWhlRodHRwOi8vZXZjYS5pdHJ1cy5jb20uY24vcHVibGljL2l0cnVzY3Js" +
			"P0NBPTFCRDQyMjBFNTBEQkMwNEIwNkFEMzk3NTQ5ODQ2QzAxQzNFOEVCRDIwDQYJ" +
			"KoZIhvcNAQELBQADggEBAGukNqe5ag5W9Vv2Tktl1KpoBLgVG1Ecy87CileduglM" +
			"Nb+Mo8hBEnPG8mMSfDHIZFz8TAOwo2fI/fjHCxil0L5Sd+rHwy+FjHw0D29uQpve" +
			"b8OCFDO0v3bF7TpCepc2TPp/DAvowpcweA8Y0HirlWjRBWBGS6jWatDmnSpeYu1D" +
			"wpzGbS60FuvBAWoss7VzijXn9bXcS0IQ6LPEPndYiHIhcygeXiS2KANLpsgLqmCk" +
			"GSHI9SHGceDtuGDkMfHo6e+xFOU062AVrH7+DVP0TBAtSodQ5KUIcW40gEDwEU0Z" +
			"k9gzTEvpj0gl9aFeyuv6NzE37apVKGWz1lJiD/9lesU=";

		String certificate1 = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDCUlJRkq6HdpZDDDRfGB708Cmg9U9iZIz4V/DIvMNoNcduWuSnLhwFXVxiSRFCaLjWuDPbvZZZ8TrrL+v5CsYunXTaGpRfKLEBSstCIDMAwtSbKpzVPnwW5IgwiYnnJdD+G5peY3mmuHd/XgbSx03qC/NCWLrlgjkypj5ew97QfQIDAQAB";

        Map<String, String> keys = getKeys();
      // String publicKey = keys.get("publicKey");
       // String privateKey = keys.get("privateKey");
		String publicKey = certificate1;
		String privateKey = privateKey1;
        String content = "我是Javen,I am Javen";
        String encrypt = encryptByPublicKey(content, publicKey);
        String decrypt = decryptByPrivateKey(encrypt, privateKey);
        System.out.println("加密之后：" + encrypt);
        System.out.println("解密之后：" + decrypt);

        System.out.println("======华丽的分割线=========");

        content = "我是Javen,I am Javen";
        encrypt = encryptByPublicKeyByWx(content, publicKey);
        decrypt = decryptByPrivateKeyByWx(encrypt, privateKey);
        System.out.println("加密之后：" + encrypt);
        System.out.println("解密之后：" + decrypt);

        //OPPO
        String sign = encryptByPrivateKey(content, privateKey);
        System.out.println("加密之后：" + sign);
        System.out.println(checkByPublicKey(content, sign, publicKey));
    }
}
