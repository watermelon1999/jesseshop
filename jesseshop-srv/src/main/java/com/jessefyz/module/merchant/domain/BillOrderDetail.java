package com.jessefyz.module.merchant.domain;

/**
 * @Author:hanguanglei
 * @Description:
 * @Date: Created in 2020-04-12 10:31
 * @Modified By:
 */

public class BillOrderDetail {
    //交易时间
    private String payTime;
    //公众账号ID
    private String wxId;
    //商户号
    private String machId;
    //特约商户号
    private String subMachId;
    //设备号
    private String deviceId;
    //微信订单号
    private String wxOderNo;
    //商户订单号
    private String machOrderNo;
    //用户标识
    private String userId;
    //交易类型
    private String payType;
    //交易状态
    private String payStatus;
    //付款银行
    private String accoutBank;
    //货币种类
    private String currency ;
    //应结订单金额
    private String shoudMoney;
    //代金券金额
    private String couponMoney;
    //微信退款单号
    private String wxRefundOrderNo;
    //商户退款单号
    private String machRefundOrderNO;
    //退款金额
    private String refundMoney;
    //充值券退款金额
    private String couponRefundMoney;
    //退款类型
    private String refundType;
    //退款状态
    private String refundStatus;
    //商品名称
    private String goodsName;
    //商户数据包
    private String machData;
    //手续费
    private String serviceMoney ;
    //费率
    private String serviceRate;
    //订单金额
    private String orderMoney;
    //申请退款金额
    private String applyRefundMoney;
    //费率备注
    private String rateMemo;


    public String getPayTime() {
        return payTime;
    }

    public void setPayTime(String payTime) {
        this.payTime = payTime;
    }

    public String getWxId() {
        return wxId;
    }

    public void setWxId(String wxId) {
        this.wxId = wxId;
    }

    public String getMachId() {
        return machId;
    }

    public void setMachId(String machId) {
        this.machId = machId;
    }

    public String getSubMachId() {
        return subMachId;
    }

    public void setSubMachId(String subMachId) {
        this.subMachId = subMachId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getWxOderNo() {
        return wxOderNo;
    }

    public void setWxOderNo(String wxOderNo) {
        this.wxOderNo = wxOderNo;
    }

    public String getMachOrderNo() {
        return machOrderNo;
    }

    public void setMachOrderNo(String machOrderNo) {
        this.machOrderNo = machOrderNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    public String getAccoutBank() {
        return accoutBank;
    }

    public void setAccoutBank(String accoutBank) {
        this.accoutBank = accoutBank;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShoudMoney() {
        return shoudMoney;
    }

    public void setShoudMoney(String shoudMoney) {
        this.shoudMoney = shoudMoney;
    }

    public String getCouponMoney() {
        return couponMoney;
    }

    public void setCouponMoney(String couponMoney) {
        this.couponMoney = couponMoney;
    }

    public String getWxRefundOrderNo() {
        return wxRefundOrderNo;
    }

    public void setWxRefundOrderNo(String wxRefundOrderNo) {
        this.wxRefundOrderNo = wxRefundOrderNo;
    }

    public String getMachRefundOrderNO() {
        return machRefundOrderNO;
    }

    public void setMachRefundOrderNO(String machRefundOrderNO) {
        this.machRefundOrderNO = machRefundOrderNO;
    }

    public String getRefundMoney() {
        return refundMoney;
    }

    public void setRefundMoney(String refundMoney) {
        this.refundMoney = refundMoney;
    }

    public String getCouponRefundMoney() {
        return couponRefundMoney;
    }

    public void setCouponRefundMoney(String couponRefundMoney) {
        this.couponRefundMoney = couponRefundMoney;
    }

    public String getRefundType() {
        return refundType;
    }

    public void setRefundType(String refundType) {
        this.refundType = refundType;
    }

    public String getRefundStatus() {
        return refundStatus;
    }

    public void setRefundStatus(String refundStatus) {
        this.refundStatus = refundStatus;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getMachData() {
        return machData;
    }

    public void setMachData(String machData) {
        this.machData = machData;
    }

    public String getServiceMoney() {
        return serviceMoney;
    }

    public void setServiceMoney(String serviceMoney) {
        this.serviceMoney = serviceMoney;
    }

    public String getServiceRate() {
        return serviceRate;
    }

    public void setServiceRate(String serviceRate) {
        this.serviceRate = serviceRate;
    }

    public String getOrderMoney() {
        return orderMoney;
    }

    public void setOrderMoney(String orderMoney) {
        this.orderMoney = orderMoney;
    }

    public String getApplyRefundMoney() {
        return applyRefundMoney;
    }

    public void setApplyRefundMoney(String applyRefundMoney) {
        this.applyRefundMoney = applyRefundMoney;
    }

    public String getRateMemo() {
        return rateMemo;
    }

    public void setRateMemo(String rateMemo) {
        this.rateMemo = rateMemo;
    }
}
