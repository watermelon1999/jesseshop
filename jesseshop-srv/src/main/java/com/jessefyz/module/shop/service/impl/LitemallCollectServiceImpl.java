package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallCollect;
import com.jessefyz.module.shop.mapper.LitemallCollectMapper;
import com.jessefyz.module.shop.service.ILitemallCollectService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 收藏Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallCollectServiceImpl extends ServiceImpl<LitemallCollectMapper,LitemallCollect>
        implements ILitemallCollectService
{
    /**
     * 查询收藏
     *
     * @param id 收藏ID
     * @return 收藏
     */
    @Override
    public LitemallCollect selectLitemallCollectById(Long id)
    {
        return baseMapper.selectLitemallCollectById(id);
    }

    /**
     * 查询收藏列表
     *
     * @param litemallCollect 收藏
     * @return 收藏
     */
    @Override
    public List<LitemallCollect> selectLitemallCollectList(LitemallCollect litemallCollect)
    {
        return baseMapper.selectLitemallCollectList(litemallCollect);
    }

    /**
     * 新增收藏
     *
     * @param litemallCollect 收藏
     * @return 结果
     */
    @Override
    public int insertLitemallCollect(LitemallCollect litemallCollect)
    {
        return baseMapper.insertLitemallCollect(litemallCollect);
    }

    /**
     * 修改收藏
     *
     * @param litemallCollect 收藏
     * @return 结果
     */
    @Override
    public int updateLitemallCollect(LitemallCollect litemallCollect)
    {
        litemallCollect.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallCollect(litemallCollect);
    }

    /**
     * 批量删除收藏
     *
     * @param ids 需要删除的收藏ID
     * @return 结果
     */
    @Override
    public int deleteLitemallCollectByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallCollectByIds(ids);
    }

    /**
     * 删除收藏信息
     *
     * @param id 收藏ID
     * @return 结果
     */
    @Override
    public int deleteLitemallCollectById(Long id)
    {
        return baseMapper.deleteLitemallCollectById(id);
    }
}
