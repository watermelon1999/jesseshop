package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.mybatis.JsonStringArrayTypeHandler;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 购物车商品对象 litemall_cart
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
@Validated
@ToString
@TableName(autoResultMap = true)
public class LitemallCart extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    private Long id;

    /** 用户表的用户ID */
    @Excel(name = "用户表的用户ID")
    private Long userId;

    /** 商品表的商品ID */
    @Excel(name = "商品表的商品ID")
    @NotNull(message = "商品ID不能为空" )
    private Long goodsId;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String goodsSn;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品货品表的货品ID */
    @Excel(name = "商品货品表的货品ID")
    @NotNull(message = "货品ID不能为空" )
    private Long productId;

    /** 商品货品的价格 */
    @Excel(name = "商品货品的价格")
    private BigDecimal price;

    /** 商品货品的数量 */
    @Excel(name = "商品货品的数量")
    @NotNull(message = "数量不能为空" )
    @Min(message = "数量不能小于1",value = 1)
    private Integer number;

    /** 商品规格值列表，采用JSON数组格式 */
    @Excel(name = "商品规格值列表，采用JSON数组格式")
    @TableField(typeHandler = JsonStringArrayTypeHandler.class)
    private String[] specifications;

    /** 购物车中商品是否选择状态 */
    @Excel(name = "购物车中商品是否选择状态")
    private Integer checked;

    /** 商品图片或者商品货品图片 */
    @Excel(name = "商品图片或者商品货品图片")
    private String picUrl;

}
