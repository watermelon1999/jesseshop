package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.exception.DataNotFoundException;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallAddress;
import com.jessefyz.module.shop.domain.LitemallRegion;
import com.jessefyz.module.shop.mapper.LitemallAddressMapper;
import com.jessefyz.module.shop.mapper.LitemallRegionMapper;
import com.jessefyz.module.shop.service.ILitemallAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * 收货地址Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallAddressServiceImpl extends ServiceImpl<LitemallAddressMapper, LitemallAddress> implements ILitemallAddressService
{
    /**
     * 查询收货地址
     *
     * @param id 收货地址ID
     * @return 收货地址
     */
    @Override
    public LitemallAddress selectLitemallAddressById(Long id)
    {
        return baseMapper.selectLitemallAddressById(id);
    }

    /**
     * 查询收货地址列表
     *
     * @param litemallAddress 收货地址
     * @return 收货地址
     */
    @Override
    public List<LitemallAddress> selectLitemallAddressList(LitemallAddress litemallAddress)
    {
        return baseMapper.selectLitemallAddressList(litemallAddress);
    }

    /**
     * 新增收货地址
     *
     * @param litemallAddress 收货地址
     * @return 结果
     */
    @Override
    public int insertLitemallAddress(LitemallAddress litemallAddress)
    {
        return baseMapper.insertLitemallAddress(litemallAddress);
    }

    /**
     * 修改收货地址
     *
     * @param litemallAddress 收货地址
     * @return 结果
     */
    @Override
    public int updateLitemallAddress(LitemallAddress litemallAddress)
    {
        litemallAddress.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallAddress(litemallAddress);
    }

    /**
     * 批量删除收货地址
     *
     * @param ids 需要删除的收货地址ID
     * @return 结果
     */
    @Override
    public int deleteLitemallAddressByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallAddressByIds(ids);
    }

    /**
     * 删除收货地址信息
     *
     * @param id 收货地址ID
     * @return 结果
     */
    @Override
    public int deleteLitemallAddressById(Long id)
    {
        return baseMapper.deleteLitemallAddressById(id);
    }

    /**
     * 获取收货地址信息，微信小程序端获取
     *
     * @param id 收货地址ID
     * @return 结果
     */
    @Override
    public LitemallAddress getDetailFromWx(Long memberId, Long id) {
        return Optional.ofNullable(this.baseMapper.getDetailFromWx(memberId,id))
                .orElseThrow(() -> new DataNotFoundException("地址不存在"));
    }

    /**
     * 新增或修改地址信息，微信小程序端发起
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long saveOrUpdateAddress(LitemallAddress address) {
        if (1 == address.getIsDefault().intValue()) {
            // 重置其他收货地址的默认选项
            this.update(new LambdaUpdateWrapper<LitemallAddress>()
                    .set(LitemallAddress::getIsDefault,0)
                    .eq(LitemallAddress::getUserId,address.getUserId())
            );
        }
        if (address.getId() == null || address.getId().equals(0L)) {
            address.setId(null);
            address.setUpdateTime(new Date());
            this.save(address);
        } else {
            LitemallAddress litemallAddress = this.getDetailFromWx(address.getUserId(), address.getId());
            address.setAddTime(litemallAddress.getAddTime());
            address.setUpdateTime(new Date());
            address.setDeleted(0);
            this.updateById(address);
        }
        return address.getId();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public  LitemallAddress query(Integer userId, Integer id) {
        return this.getOne(new LambdaQueryWrapper<LitemallAddress>()
            .eq(LitemallAddress::getId,id)
            .eq(LitemallAddress::getUserId,userId)
        ,false);
    }
}
