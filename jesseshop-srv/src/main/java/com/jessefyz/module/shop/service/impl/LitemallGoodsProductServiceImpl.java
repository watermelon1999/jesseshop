package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallGoodsProduct;
import com.jessefyz.module.shop.domain.LitemallOrder;
import com.jessefyz.module.shop.mapper.LitemallGoodsProductMapper;
import com.jessefyz.module.shop.mapper.LitemallOrderMapper;
import com.jessefyz.module.shop.service.ILitemallGoodsProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商品货品Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallGoodsProductServiceImpl extends ServiceImpl<LitemallGoodsProductMapper, LitemallGoodsProduct> implements ILitemallGoodsProductService
{
    /**
     * 查询商品货品
     *
     * @param id 商品货品ID
     * @return 商品货品
     */
    @Override
    public LitemallGoodsProduct selectLitemallGoodsProductById(Long id)
    {
        return baseMapper.selectLitemallGoodsProductById(id);
    }

    /**
     * 查询商品货品列表
     *
     * @param litemallGoodsProduct 商品货品
     * @return 商品货品
     */
    @Override
    public List<LitemallGoodsProduct> selectLitemallGoodsProductList(LitemallGoodsProduct litemallGoodsProduct)
    {
        return baseMapper.selectLitemallGoodsProductList(litemallGoodsProduct);
    }

    @Override
    public List<LitemallGoodsProduct> queryByGid(Long goodsId) {
        return baseMapper.queryByGid(goodsId);
    }

    /**
     * 新增商品货品
     *
     * @param litemallGoodsProduct 商品货品
     * @return 结果
     */
    @Override
    public int insertLitemallGoodsProduct(LitemallGoodsProduct litemallGoodsProduct)
    {
        return baseMapper.insertLitemallGoodsProduct(litemallGoodsProduct);
    }

    /**
     * 修改商品货品
     *
     * @param litemallGoodsProduct 商品货品
     * @return 结果
     */
    @Override
    public int updateLitemallGoodsProduct(LitemallGoodsProduct litemallGoodsProduct)
    {
        litemallGoodsProduct.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallGoodsProduct(litemallGoodsProduct);
    }

    /**
     * 批量删除商品货品
     *
     * @param ids 需要删除的商品货品ID
     * @return 结果
     */
    @Override
    public int deleteLitemallGoodsProductByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallGoodsProductByIds(ids);
    }

    /**
     * 删除商品货品信息
     *
     * @param id 商品货品ID
     * @return 结果
     */
    @Override
    public int deleteLitemallGoodsProductById(Long id)
    {
        return baseMapper.deleteLitemallGoodsProductById(id);
    }

    @Override
    public int addStock(Long productId, Integer number)
    {
        return baseMapper.addStock(productId, number);
    }

    @Override
    public int reduceStock(Long id, Integer num){
        return this.baseMapper.reduceStock(id, num);
    }

}
