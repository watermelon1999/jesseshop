package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.common.utils.GrouponConstant;
import com.jessefyz.module.shop.domain.LitemallGroupon;
import com.jessefyz.module.shop.mapper.LitemallGrouponMapper;
import com.jessefyz.module.shop.service.ILitemallGrouponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 团购活动Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallGrouponServiceImpl extends ServiceImpl<LitemallGrouponMapper,LitemallGroupon> implements ILitemallGrouponService
{

    /**
     * 查询团购活动
     *
     * @param id 团购活动ID
     * @return 团购活动
     */
    @Override
    public LitemallGroupon selectLitemallGrouponById(Long id)
    {
        return baseMapper.selectLitemallGrouponById(id);
    }

    /**
     * 查询团购活动列表
     *
     * @param litemallGroupon 团购活动
     * @return 团购活动
     */
    @Override
    public List<LitemallGroupon> selectLitemallGrouponList(LitemallGroupon litemallGroupon)
    {
        return baseMapper.selectLitemallGrouponList(litemallGroupon);
    }

    /**
     * 新增团购活动
     *
     * @param litemallGroupon 团购活动
     * @return 结果
     */
    @Override
    public int insertLitemallGroupon(LitemallGroupon litemallGroupon)
    {
        return baseMapper.insertLitemallGroupon(litemallGroupon);
    }

    /**
     * 修改团购活动
     *
     * @param litemallGroupon 团购活动
     * @return 结果
     */
    @Override
    public int updateLitemallGroupon(LitemallGroupon litemallGroupon)
    {
        litemallGroupon.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallGroupon(litemallGroupon);
    }

    /**
     * 批量删除团购活动
     *
     * @param ids 需要删除的团购活动ID
     * @return 结果
     */
    @Override
    public int deleteLitemallGrouponByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallGrouponByIds(ids);
    }

    /**
     * 删除团购活动信息
     *
     * @param id 团购活动ID
     * @return 结果
     */
    @Override
    public int deleteLitemallGrouponById(Long id)
    {
        return baseMapper.deleteLitemallGrouponById(id);
    }

    @Override
    public LitemallGroupon queryByOrderId(Long orderId) {
        return this.getOne(new LambdaQueryWrapper<LitemallGroupon>()
                .eq(LitemallGroupon::getOrderId,orderId)
                .last("limit 1")
                ,false);
    }

    @Override
    public int countGroupon(Long grouponId) {
        return this.count(new LambdaQueryWrapper<LitemallGroupon>()
            .eq(LitemallGroupon::getGrouponId,grouponId)
            .ne(LitemallGroupon::getStatus, GrouponConstant.STATUS_NONE)
        );
    }

    @Override
    public boolean hasJoin(Integer userId, Long grouponId) {
        return this.count(new LambdaQueryWrapper<LitemallGroupon>()
            .eq(LitemallGroupon::getUserId,userId)
            .ne(LitemallGroupon::getStatus, GrouponConstant.STATUS_NONE)
        ) > 0;
    }

    /**
     * 根据ID查询记录
     *
     * @param userId
     * @param id
     * @return
     */
    @Override
    public LitemallGroupon queryById(Integer userId, Long id) {
        return this.getOne(new LambdaQueryWrapper<LitemallGroupon>()
            .eq(LitemallGroupon::getId,id)
            .eq(LitemallGroupon::getUserId,userId)
        );
    }

    @Override
    public List<LitemallGroupon> queryJoinRecord(Long grouponId) {
        return this.list(new LambdaQueryWrapper<LitemallGroupon>()
            .eq(LitemallGroupon::getGrouponId,grouponId)
            .ne(LitemallGroupon::getStatus,GrouponConstant.STATUS_NONE)
            .orderByDesc(BaseShopEntity::getAddTime)
        );
    }
}
