package com.jessefyz.module.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jessefyz.module.shop.domain.LitemallGoodsProduct;
import com.jessefyz.module.shop.domain.LitemallOrder;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品货品Mapper接口
 *
 * @author ruoyi
 * @date 2020-03-11
 */
public interface LitemallGoodsProductMapper extends BaseMapper<LitemallGoodsProduct>
{
    /**
     * 查询商品货品
     *
     * @param id 商品货品ID
     * @return 商品货品
     */
    public LitemallGoodsProduct selectLitemallGoodsProductById(Long id);

    /**
     * 查询商品货品列表
     *
     * @param litemallGoodsProduct 商品货品
     * @return 商品货品集合
     */
    public List<LitemallGoodsProduct> selectLitemallGoodsProductList(LitemallGoodsProduct litemallGoodsProduct);



    List<LitemallGoodsProduct>  queryByGid(Long goodsId);

    /**
     * 新增商品货品
     *
     * @param litemallGoodsProduct 商品货品
     * @return 结果
     */
    public int insertLitemallGoodsProduct(LitemallGoodsProduct litemallGoodsProduct);

    /**
     * 修改商品货品
     *
     * @param litemallGoodsProduct 商品货品
     * @return 结果
     */
    public int updateLitemallGoodsProduct(LitemallGoodsProduct litemallGoodsProduct);

    /**
     * 删除商品货品
     *
     * @param id 商品货品ID
     * @return 结果
     */
    public int deleteLitemallGoodsProductById(Long id);

    /**
     * 批量删除商品货品
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteLitemallGoodsProductByIds(Long[] ids);

    public int addStock(@Param("id") Long productId, @Param("num") Integer number);

    public int reduceStock(@Param("id") Long id, @Param("num") Integer num);
}
