package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 收货地址对象 litemall_address
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Builder
@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor
public class LitemallAddress extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @ApiModelProperty("地址id，新增时该字段可不传")
    private Long id;

    /** 收货人名称 */
    @ApiModelProperty("收货人名称")
    @Excel(name = "收货人名称")
    @NotBlank(message = "请输入收货人姓名")
    private String name;

    /** 手机号码 */
    @ApiModelProperty("手机号码")
    @Excel(name = "手机号码")
    @NotBlank(message = "请输入收货人手机号码")
    private String tel;

    /** 用户表的用户ID */
    @ApiModelProperty(value = "微信用户id",hidden = true)
    @Excel(name = "用户表的用户ID")
    private Long userId;

    /** 行政区域表的省ID */
    @ApiModelProperty(value = "行政区域表的省ID",required = true)
    @NotBlank(message = "请选择省份")
    @Excel(name = "行政区域表的省ID")
    private String province;

    /** 行政区域表的市ID */
    @ApiModelProperty(value = "行政区域表的市ID",required = true)
    @NotBlank(message = "请选择城市")
    @Excel(name = "行政区域表的市ID")
    private String city;

    /** 行政区域表的区县ID */
    @ApiModelProperty(value = "行政区域表的区县ID",required = true)
    @NotBlank(message = "请选择区域")
    private String county;

    /** 详细收货地址 */
    @Excel(name = "详细收货地址")
    @NotBlank(message = "请输入详细地址")
    private String addressDetail;
    /** 地区编码 */
    @Excel(name = "地区编码")
    private String areaCode;

    /** 邮政编码 */
    @Excel(name = "邮政编码")
    private String postalCode;

    /** 是否默认地址 */
    @Excel(name = "是否默认地址")
    @ApiModelProperty(value = "是否默认地址 0否 1是",required = true)
    @NotNull(message = "请选择是否默认")
    private Integer isDefault;

    @Excel(name = "省份")
    @TableField(exist = false)
    private String provinceName;

    @Excel(name = "城市")
    @TableField(exist = false)
    private String cityName;

    @Excel(name = "区域")
    @TableField(exist = false)
    private String countyName;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("userId", getUserId())
            .append("province", getProvince())
            .append("city", getCity())
            .append("county", getCounty())
            .append("addressDetail", getAddressDetail())
            .append("areaCode", getAreaCode())
            .append("postalCode", getPostalCode())
            .append("tel", getTel())
            .append("isDefault", getIsDefault())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
