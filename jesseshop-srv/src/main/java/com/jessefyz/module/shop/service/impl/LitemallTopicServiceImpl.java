package com.jessefyz.module.shop.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallTopic;
import com.jessefyz.module.shop.mapper.LitemallTopicMapper;
import com.jessefyz.module.shop.service.ILitemallTopicService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 专题Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallTopicServiceImpl extends ServiceImpl<LitemallTopicMapper, LitemallTopic> implements ILitemallTopicService
{

    /**
     * 查询专题
     *
     * @param id 专题ID
     * @return 专题
     */
    @Override
    public LitemallTopic selectLitemallTopicById(Long id)
    {
        return baseMapper.selectLitemallTopicById(id);
    }

    /**
     * 查询专题列表
     *
     * @param litemallTopic 专题
     * @return 专题
     */
    @Override
    public List<LitemallTopic> selectLitemallTopicList(LitemallTopic litemallTopic)
    {
        return baseMapper.selectLitemallTopicList(litemallTopic);
    }

    /**
     * 新增专题
     *
     * @param litemallTopic 专题
     * @return 结果
     */
    @Override
    public int insertLitemallTopic(LitemallTopic litemallTopic)
    {
        return baseMapper.insertLitemallTopic(litemallTopic);
    }

    /**
     * 修改专题
     *
     * @param litemallTopic 专题
     * @return 结果
     */
    @Override
    public int updateLitemallTopic(LitemallTopic litemallTopic)
    {
        litemallTopic.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallTopic(litemallTopic);
    }

    /**
     * 批量删除专题
     *
     * @param ids 需要删除的专题ID
     * @return 结果
     */
    @Override
    public int deleteLitemallTopicByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallTopicByIds(ids);
    }

    /**
     * 删除专题信息
     *
     * @param id 专题ID
     * @return 结果
     */
    @Override
    public int deleteLitemallTopicById(Long id)
    {
        return baseMapper.deleteLitemallTopicById(id);
    }

    @Override
    public List<LitemallTopic> queryList(int offset, int limit, String sort, String order) {
        PageHelper.startPage(offset, limit);
        return this.list(new LambdaQueryWrapper<LitemallTopic>()
        );
    }

    @Override
    public List<LitemallTopic> queryRelatedList(Integer id, int offset, int limit) {
        List<LitemallTopic> topics = this.list(new LambdaQueryWrapper<LitemallTopic>()
            .eq(LitemallTopic::getId,id)
        );
        if (CollectionUtil.isEmpty(topics)) {
            return queryList(offset, limit, "add_time", "desc");
        }
        LitemallTopic topic = topics.get(0);

        PageHelper.startPage(offset, limit);
        List<LitemallTopic> relateds = this.list(new LambdaQueryWrapper<LitemallTopic>()
                .ne(LitemallTopic::getId,id)
        );
        if (relateds.size() != 0) {
            return relateds;
        }
        return queryList(offset, limit, "add_time", "desc");
    }
}
