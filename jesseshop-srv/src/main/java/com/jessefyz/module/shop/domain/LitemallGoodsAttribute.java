package com.jessefyz.module.shop.domain;

import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 商品参数对象 litemall_goods_attribute
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallGoodsAttribute extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 商品表的商品ID */
    @Excel(name = "商品表的商品ID")
    private Long goodsId;

    /** 商品参数名称 */
    @Excel(name = "商品参数名称")
    private String attribute;

    /** 商品参数值 */
    @Excel(name = "商品参数值")
    private String value;


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("attribute", getAttribute())
            .append("value", getValue())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
