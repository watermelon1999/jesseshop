package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.module.shop.common.utils.OrderHandleOption;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 订单对象 litemall_order
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallOrder extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户表的用户ID */
    @Excel(name = "用户表的用户ID")
    private Long userId;

    /** 订单编号 */
    @Excel(name = "订单编号")
    private String orderSn;

    /** 订单状态 */
    @Excel(name = "订单状态")
    private Short orderStatus;

    /** 订单状态 */
    @Excel(name = "售后状态 0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消")
    private Short aftersaleStatus;

    /** 收货人名称 */
    @Excel(name = "收货人名称")
    private String consignee;

    /** 收货人手机号 */
    @Excel(name = "收货人手机号")
    private String mobile;

    /** 收货具体地址 */
    @Excel(name = "收货具体地址")
    private String address;

    /** 用户订单留言 */
    @Excel(name = "用户订单留言")
    private String message;

    /** 商品总费用 */
    @Excel(name = "商品总费用")
    private BigDecimal goodsPrice;

    /** 配送费用 */
    @Excel(name = "配送费用")
    private BigDecimal freightPrice;

    /** 优惠券减免 */
    @Excel(name = "优惠券减免")
    private BigDecimal couponPrice;

    /** 用户积分减免 */
    @Excel(name = "用户积分减免")
    private BigDecimal integralPrice;

    /** 团购优惠价减免 */
    @Excel(name = "团购优惠价减免")
    private BigDecimal grouponPrice;

    /** 订单费用， = goods_price + freight_price - coupon_price*/
    @Excel(name = "订单金额")
    private BigDecimal orderPrice;

    /** 实付费用， = order_price - integral_price */
    @Excel(name = "实付金额")
    private BigDecimal actualPrice;

    /** 微信付款编号 */
    @Excel(name = "微信付款编号")
    private String payId;

    /** 微信付款时间 */
    @Excel(name = "微信付款时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    /** 发货编号 */
    @Excel(name = "发货编号")
    private String shipSn;

    /** 发货快递公司 */
    @Excel(name = "发货快递公司")
    private String shipChannel;

    /** 发货开始时间 */
    @Excel(name = "发货开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date shipTime;

    /** 实际退款金额，（有可能退款金额小于实际支付金额） */
    @Excel(name = "实际退款金额，", readConverterExp = "有=可能退款金额小于实际支付金额")
    private Double refundAmount;

    /** 退款方式 */
    @Excel(name = "退款方式")
    private Integer refundType;

    /** 退款备注 */
    @Excel(name = "退款备注")
    private String refundContent;

    /** 退款时间 */
    @Excel(name = "退款时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date refundTime;

    /** 用户确认收货时间 */
    @Excel(name = "用户确认收货时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date confirmTime;

    /** 待评价订单商品数量 */
    @Excel(name = "待评价订单商品数量")
    private Integer comments;

    /** 订单关闭时间 */
    @Excel(name = "订单关闭时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date closeTime;

    /** 下单时间 */
    @Excel(name = "下单时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    @TableField(exist = false)
    private List<LitemallOrderGoods> goodsVoList;

    /**用户昵称*/
    @TableField(exist = false)
    private String memberNickname;

    /**用户头像*/
    @TableField(exist = false)
    private String memberPic;

    /**状态可操作选项*/
    @TableField(exist = false)
    private OrderHandleOption orderHandleOption;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("orderSn", getOrderSn())
            .append("orderStatus", getOrderStatus())
            .append("consignee", getConsignee())
            .append("mobile", getMobile())
            .append("address", getAddress())
            .append("message", getMessage())
            .append("goodsPrice", getGoodsPrice())
            .append("freightPrice", getFreightPrice())
            .append("couponPrice", getCouponPrice())
            .append("integralPrice", getIntegralPrice())
            .append("grouponPrice", getGrouponPrice())
            .append("orderPrice", getOrderPrice())
            .append("actualPrice", getActualPrice())
            .append("payId", getPayId())
            .append("payTime", getPayTime())
            .append("shipSn", getShipSn())
            .append("shipChannel", getShipChannel())
            .append("shipTime", getShipTime())
            .append("refundAmount", getRefundAmount())
            .append("refundType", getRefundType())
            .append("refundContent", getRefundContent())
            .append("refundTime", getRefundTime())
            .append("confirmTime", getConfirmTime())
            .append("comments", getComments())
            .append("closeTime", getCloseTime())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
