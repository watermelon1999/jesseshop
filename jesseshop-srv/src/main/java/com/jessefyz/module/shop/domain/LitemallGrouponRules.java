package com.jessefyz.module.shop.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 团购规则对象 litemall_groupon_rules
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallGrouponRules extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 商品表的商品ID */
    @Excel(name = "商品表的商品ID")
    private Long goodsId;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String goodsName;

    /** 商品图片或者商品货品图片 */
    @Excel(name = "商品图片或者商品货品图片")
    private String picUrl;

    /** 优惠金额 */
    @Excel(name = "优惠金额")
    private BigDecimal discount;

    /** 达到优惠条件的人数 */
    @Excel(name = "达到优惠条件的人数")
    private Long discountMember;

    /** 团购过期时间 */
    @Excel(name = "团购过期时间", width = 30, dateFormat = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date expireTime;

    /** 团购规则状态，正常上线则0，到期自动下线则1，管理手动下线则2 */
    @Excel(name = "团购规则状态，正常上线则0，到期自动下线则1，管理手动下线则2")
    private Short status;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("goodsName", getGoodsName())
            .append("picUrl", getPicUrl())
            .append("discount", getDiscount())
            .append("discountMember", getDiscountMember())
            .append("expireTime", getExpireTime())
            .append("status", getStatus())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
