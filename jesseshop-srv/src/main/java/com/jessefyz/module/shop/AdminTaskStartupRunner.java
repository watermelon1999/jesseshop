package com.jessefyz.module.shop;

import com.jessefyz.common.task.TaskService;
import com.jessefyz.common.utils.GrouponConstant;
import com.jessefyz.module.shop.domain.LitemallGrouponRules;
import com.jessefyz.module.shop.service.ILitemallGrouponRulesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

@Component
public class AdminTaskStartupRunner implements ApplicationRunner {

    @Autowired
    private ILitemallGrouponRulesService rulesService;
    @Autowired
    private TaskService taskService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LitemallGrouponRules litemallGrouponRules=new LitemallGrouponRules();
        litemallGrouponRules.setStatus(GrouponConstant.RULE_STATUS_ON);
        List<LitemallGrouponRules> grouponRulesList = rulesService.selectLitemallGrouponRulesList(litemallGrouponRules);
        for(LitemallGrouponRules grouponRules : grouponRulesList){
            Date now = new Date();
            Date expire =  grouponRules.getExpireTime();
            if(expire.getTime() < now.getTime()) {
                // 已经过期，则加入延迟队列
                taskService.addTask(new GrouponRuleExpiredTask(grouponRules.getId(), 0));
            }
            else{
                // 还没过期，则加入延迟队列
//                long delay = ChronoUnit.MILLIS.between(now, expire);
                long delay = expire.getTime() - now.getTime();
                taskService.addTask(new GrouponRuleExpiredTask(grouponRules.getId(), delay));
            }
        }
    }
}
