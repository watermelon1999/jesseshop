package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallFootprint;
import com.jessefyz.module.shop.mapper.LitemallFootprintMapper;
import com.jessefyz.module.shop.service.ILitemallFootprintService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户浏览足迹Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallFootprintServiceImpl extends ServiceImpl<LitemallFootprintMapper,LitemallFootprint> implements ILitemallFootprintService
{

    /**
     * 查询用户浏览足迹
     *
     * @param id 用户浏览足迹ID
     * @return 用户浏览足迹
     */
    @Override
    public LitemallFootprint selectLitemallFootprintById(Long id)
    {
        return baseMapper.selectLitemallFootprintById(id);
    }

    /**
     * 查询用户浏览足迹列表
     *
     * @param litemallFootprint 用户浏览足迹
     * @return 用户浏览足迹
     */
    @Override
    public List<LitemallFootprint> selectLitemallFootprintList(LitemallFootprint litemallFootprint)
    {
        return baseMapper.selectLitemallFootprintList(litemallFootprint);
    }

    /**
     * 新增用户浏览足迹
     *
     * @param litemallFootprint 用户浏览足迹
     * @return 结果
     */
    @Override
    public int insertLitemallFootprint(LitemallFootprint litemallFootprint)
    {
        return baseMapper.insertLitemallFootprint(litemallFootprint);
    }

    /**
     * 修改用户浏览足迹
     *
     * @param litemallFootprint 用户浏览足迹
     * @return 结果
     */
    @Override
    public int updateLitemallFootprint(LitemallFootprint litemallFootprint)
    {
        litemallFootprint.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallFootprint(litemallFootprint);
    }

    /**
     * 批量删除用户浏览足迹
     *
     * @param ids 需要删除的用户浏览足迹ID
     * @return 结果
     */
    @Override
    public int deleteLitemallFootprintByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallFootprintByIds(ids);
    }

    /**
     * 删除用户浏览足迹信息
     *
     * @param id 用户浏览足迹ID
     * @return 结果
     */
    @Override
    public int deleteLitemallFootprintById(Long id)
    {
        return baseMapper.deleteLitemallFootprintById(id);
    }
}
