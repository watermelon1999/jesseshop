package com.jessefyz.module.shop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jessefyz.module.shop.domain.LitemallOrder;
import com.jessefyz.module.shop.req.OrderPay;
import com.jessefyz.module.shop.req.OrderRefund;
import com.jessefyz.module.shop.req.OrderShip;

import java.util.List;
import java.util.Map;

/**
 * 订单Service接口
 *
 * @author ruoyi
 * @date 2020-03-11
 */
public interface ILitemallOrderService extends IService<LitemallOrder>
{
    /**
     * 查询订单
     *
     * @param id 订单ID
     * @return 订单
     */
    public LitemallOrder selectLitemallOrderById(Long id);

    /**
     * 查询订单列表
     *
     * @param litemallOrder 订单
     * @return 订单集合
     */
    public List<LitemallOrder> selectLitemallOrderList(LitemallOrder litemallOrder);

    /**
     * 新增订单
     *
     * @param litemallOrder 订单
     * @return 结果
     */
    public int insertLitemallOrder(LitemallOrder litemallOrder);

    /**
     * 修改订单
     *
     * @param litemallOrder 订单
     * @return 结果
     */
    public int updateLitemallOrder(LitemallOrder litemallOrder);

    /**
     * 批量删除订单
     *
     * @param ids 需要删除的订单ID
     * @return 结果
     */
    int deleteLitemallOrderByIds(Long[] ids);

    /**
     * 删除订单信息
     *
     * @param id 订单ID
     * @return 结果
     */
     int deleteLitemallOrderById(Long id);

    void ship(OrderShip orderShip);

    void pay(OrderPay orderPay);

    void refund(OrderRefund orderRefund);

    LitemallOrder findById(Integer userId, Integer orderId);

    List<LitemallOrder> queryByOrderStatus(Integer userId, List<Integer> orderStatus, Integer page, Integer limit, String sort, String order);

    int updateWithOptimisticLocker(LitemallOrder order);

    String generateOrderSn(Integer userId);

    LitemallOrder findBySn(String orderSn);

    Map<Object, Object> orderInfo(Long id);

    List<LitemallOrder> queryUnconfirm(Integer orderUnconfirm);

    List<LitemallOrder> queryComment(Integer orderComment);
}
