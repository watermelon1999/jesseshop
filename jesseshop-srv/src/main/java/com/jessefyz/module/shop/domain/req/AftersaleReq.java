package com.jessefyz.module.shop.domain.req;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jessefyz.common.utils.StringUtils;
import com.jessefyz.module.base.domain.req.BaseReq;
import com.jessefyz.module.shop.domain.Aftersale;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName AftersaleReq
 * @Author jessefyz
 * @Date 2021-09-08
 * @Version 1.0.0
 * @Description 列表查询信息体
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
public class AftersaleReq extends BaseReq {
    private static final long serialVersionUID = 1L;

    /** 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消 */
    private Integer status;

    /**
    * @Description 生成查询wrapper
    * @Author Jesse(xxxxxxxx@qq.com)
    * @Date 2020-11-04 16:04
    * @Param [lqw]
    * @Param [isVO] 是否vo查询体，true的话走的是mapper.xml里面的查询语句，不过查询语法还是mybatis-plus，主要用于某些关联查询出一些非数据表字段
    * @Return void
    */
    public void generatorQuery(QueryWrapper<Aftersale> lqw,boolean isVo) {
        String alias = "";
        if (isVo) {
            alias = "o.";
            lqw.eq(alias+"del_status",0);
        }
        if (null != getStatus()) {
            lqw.eq(alias+"status",getStatus());
        }
    }
}
