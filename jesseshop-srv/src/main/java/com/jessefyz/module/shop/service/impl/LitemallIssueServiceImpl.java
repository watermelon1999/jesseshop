package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallIssue;
import com.jessefyz.module.shop.mapper.LitemallIssueMapper;
import com.jessefyz.module.shop.service.ILitemallIssueService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 常见问题Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallIssueServiceImpl extends ServiceImpl<LitemallIssueMapper,LitemallIssue> implements ILitemallIssueService
{

    /**
     * 查询常见问题
     *
     * @param id 常见问题ID
     * @return 常见问题
     */
    @Override
    public LitemallIssue selectLitemallIssueById(Long id)
    {
        return baseMapper.selectLitemallIssueById(id);
    }

    /**
     * 查询常见问题列表
     *
     * @param litemallIssue 常见问题
     * @return 常见问题
     */
    @Override
    public List<LitemallIssue> selectLitemallIssueList(LitemallIssue litemallIssue)
    {
        return baseMapper.selectLitemallIssueList(litemallIssue);
    }

    /**
     * 新增常见问题
     *
     * @param litemallIssue 常见问题
     * @return 结果
     */
    @Override
    public int insertLitemallIssue(LitemallIssue litemallIssue)
    {
        return baseMapper.insertLitemallIssue(litemallIssue);
    }

    /**
     * 修改常见问题
     *
     * @param litemallIssue 常见问题
     * @return 结果
     */
    @Override
    public int updateLitemallIssue(LitemallIssue litemallIssue)
    {
        litemallIssue.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallIssue(litemallIssue);
    }

    /**
     * 批量删除常见问题
     *
     * @param ids 需要删除的常见问题ID
     * @return 结果
     */
    @Override
    public int deleteLitemallIssueByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallIssueByIds(ids);
    }

    /**
     * 删除常见问题信息
     *
     * @param id 常见问题ID
     * @return 结果
     */
    @Override
    public int deleteLitemallIssueById(Long id)
    {
        return baseMapper.deleteLitemallIssueById(id);
    }

    @Override
    public List<LitemallIssue> querySelective(String question, Integer page, Integer limit, String sort, String order) {
        PageHelper.startPage(page, limit);
        return this.list(new LambdaQueryWrapper<LitemallIssue>()
            .like(!StringUtils.isEmpty(question),LitemallIssue::getQuestion,question)
            .orderByDesc(LitemallIssue::getAddTime)
        );
    }


}
