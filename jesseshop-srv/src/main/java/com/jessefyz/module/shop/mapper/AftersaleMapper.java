package com.jessefyz.module.shop.mapper;

import com.jessefyz.module.shop.domain.Aftersale;
import com.jessefyz.module.shop.domain.vo.AftersaleVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.util.List;
/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName Aftersale
 * @Author jessefyz
 * @Date 2021-09-08
 * @Version 1.0.0
 * @Description 售后Mapper接口
 */
public interface AftersaleMapper extends BaseMapper<Aftersale> {

    List<AftersaleVo> listVo(@Param("ew") QueryWrapper<Aftersale> lqw);

    AftersaleVo getVo(@Param("id")Long id);
}
