package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 品牌商对象 litemall_brand
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallBrand extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 品牌商名称 */
    @Excel(name = "品牌商名称")
    private String name;

    /** 品牌商简介 */
    @Excel(name = "品牌商简介")
    @TableField("`desc`")
    private String desc;

    /** 品牌商页的品牌商图片 */
    @Excel(name = "品牌商页的品牌商图片")
    private String picUrl;

    /** $column.columnComment */
    @Excel(name = "品牌商页的品牌商图片")
    private Integer sortOrder;

    /** 品牌商的商品低价，仅用于页面展示 */
    @Excel(name = "品牌商的商品低价，仅用于页面展示")
    private BigDecimal floorPrice;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("desc", getDesc())
            .append("picUrl", getPicUrl())
            .append("sortOrder", getSortOrder())
            .append("floorPrice", getFloorPrice())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
