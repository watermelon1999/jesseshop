package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallAd;
import com.jessefyz.module.shop.mapper.LitemallAdMapper;
import com.jessefyz.module.shop.service.ILitemallAdService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 广告Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallAdServiceImpl extends ServiceImpl<LitemallAdMapper,LitemallAd> implements ILitemallAdService
{

    /**
     * 查询广告
     *
     * @param id 广告ID
     * @return 广告
     */
    @Override
    public LitemallAd selectLitemallAdById(Long id)
    {
        return baseMapper.selectLitemallAdById(id);
    }

    /**
     * 查询广告列表
     *
     * @param litemallAd 广告
     * @return 广告
     */
    @Override
    public List<LitemallAd> selectLitemallAdList(LitemallAd litemallAd)
    {
        return baseMapper.selectLitemallAdList(litemallAd);
    }

    /**
     * 新增广告
     *
     * @param litemallAd 广告
     * @return 结果
     */
    @Override
    public int insertLitemallAd(LitemallAd litemallAd)
    {
        return baseMapper.insertLitemallAd(litemallAd);
    }

    /**
     * 修改广告
     *
     * @param litemallAd 广告
     * @return 结果
     */
    @Override
    public int updateLitemallAd(LitemallAd litemallAd)
    {
        litemallAd.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallAd(litemallAd);
    }

    /**
     * 批量删除广告
     *
     * @param ids 需要删除的广告ID
     * @return 结果
     */
    @Override
    public int deleteLitemallAdByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallAdByIds(ids);
    }

    /**
     * 删除广告信息
     *
     * @param id 广告ID
     * @return 结果
     */
    @Override
    public int deleteLitemallAdById(Long id)
    {
        return baseMapper.deleteLitemallAdById(id);
    }
}
