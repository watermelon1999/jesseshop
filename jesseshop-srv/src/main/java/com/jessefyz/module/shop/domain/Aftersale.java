package com.jessefyz.module.shop.domain;

import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableName;

import java.math.BigDecimal;
import java.util.Date;

/**
 * jessefyz
 * ==================================================================
 * CopyRight © 2017-2099 jessefyz工作室
 * 官网地址：http://www.mdsoftware.cn
 * 技术支持：158899639xx
 * ------------------------------------------------------------------
 * 这不是一个自由软件！未经本公司授权您只能在不用于商业目的的前提下
 * 对本程序代码进行修改和使用；不允许对本程序代码以任何目的的再发布。
 * ==================================================================
 *
 * @ClassName Aftersale
 * @Author jessefyz
 * @Date 2021-09-08
 * @Version 1.0.0
 * @Description 售后 PO对象
 */
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@Accessors(chain = true)
@TableName("litemall_aftersale")
public class Aftersale extends BaseShopEntity {
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 售后编号 */
    private String aftersaleSn;
    /** 订单ID */
    private Long orderId;
    /** 用户ID */
    private Long userId;
    /** 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款 */
    private Integer type;
    /** 退款原因 */
    private String reason;
    /** 退款金额 */
    private BigDecimal amount;
    /** 退款凭证图片链接数组 */
    private String pictures;
    /** 退款说明 */
    private String comment;
    /** 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消 */
    private Short status;
    /** 管理员操作时间 */
    private Date handleTime;
}
