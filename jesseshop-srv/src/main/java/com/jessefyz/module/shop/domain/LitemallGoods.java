package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.mybatis.JsonStringArrayTypeHandler;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品基本信息对象 litemall_goods
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
@TableName(autoResultMap = true)
public class LitemallGoods extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 商品编号 */
    @Excel(name = "商品编号")
    private String goodsSn;

    /** 商品名称 */
    @Excel(name = "商品名称")
    private String name;

    /** 商品所属类目ID */
    @Excel(name = "商品所属类目ID")
    private Long categoryId;

    /** $column.columnComment */
    @Excel(name = "商品所属类目ID")
    private Long brandId;

    /** 商品宣传图片列表，采用JSON数组格式 */
    @Excel(name = "商品宣传图片列表，采用JSON数组格式")
    @TableField(typeHandler = JsonStringArrayTypeHandler.class)
    private String[] gallery;

    /** 商品关键字，采用逗号间隔 */
    @Excel(name = "商品关键字，采用逗号间隔")
    private String keywords;

    /** 商品简介 */
    @Excel(name = "商品简介")
    private String brief;

    /** 是否上架 */
    @Excel(name = "是否上架")
    private Integer isOnSale;

    /** $column.columnComment */
    @Excel(name = "是否上架")
    private Integer sortOrder;

    /** 商品页面商品图片 */
    @Excel(name = "商品页面商品图片")
    private String picUrl;

    /** 商品分享朋友圈图片 */
    @Excel(name = "商品分享朋友圈图片")
    private String shareUrl;

    /** 是否新品首发，如果设置则可以在新品首发页面展示 */
    @Excel(name = "是否新品首发，如果设置则可以在新品首发页面展示")
    private Integer isNew;

    /** 是否人气推荐，如果设置则可以在人气推荐页面展示 */
    @Excel(name = "是否人气推荐，如果设置则可以在人气推荐页面展示")
    private Integer isHot;

    /** 商品单位，例如件、盒 */
    @Excel(name = "商品单位，例如件、盒")
    private String unit;

    /** 专柜价格 */
    @Excel(name = "专柜价格")
    private BigDecimal counterPrice;

    /** 零售价格 */
    @Excel(name = "零售价格")
    private BigDecimal retailPrice;

    /** 商品详细介绍，是富文本格式 */
    @Excel(name = "商品详细介绍，是富文本格式")
    private String detail;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsSn", getGoodsSn())
            .append("name", getName())
            .append("categoryId", getCategoryId())
            .append("brandId", getBrandId())
            .append("gallery", getGallery())
            .append("keywords", getKeywords())
            .append("brief", getBrief())
            .append("isOnSale", getIsOnSale())
            .append("sortOrder", getSortOrder())
            .append("picUrl", getPicUrl())
            .append("shareUrl", getShareUrl())
            .append("isNew", getIsNew())
            .append("isHot", getIsHot())
            .append("unit", getUnit())
            .append("counterPrice", getCounterPrice())
            .append("retailPrice", getRetailPrice())
            .append("detail", getDetail())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
