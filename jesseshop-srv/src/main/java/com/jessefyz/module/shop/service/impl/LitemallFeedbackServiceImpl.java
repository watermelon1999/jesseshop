package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallFeedback;
import com.jessefyz.module.shop.mapper.LitemallFeedbackMapper;
import com.jessefyz.module.shop.service.ILitemallFeedbackService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 意见反馈Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallFeedbackServiceImpl extends ServiceImpl<LitemallFeedbackMapper,LitemallFeedback> implements ILitemallFeedbackService
{

    /**
     * 查询意见反馈
     *
     * @param id 意见反馈ID
     * @return 意见反馈
     */
    @Override
    public LitemallFeedback selectLitemallFeedbackById(Long id)
    {
        return baseMapper.selectLitemallFeedbackById(id);
    }

    /**
     * 查询意见反馈列表
     *
     * @param litemallFeedback 意见反馈
     * @return 意见反馈
     */
    @Override
    public List<LitemallFeedback> selectLitemallFeedbackList(LitemallFeedback litemallFeedback)
    {
        return baseMapper.selectLitemallFeedbackList(litemallFeedback);
    }

    /**
     * 新增意见反馈
     *
     * @param litemallFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int insertLitemallFeedback(LitemallFeedback litemallFeedback)
    {
        return baseMapper.insertLitemallFeedback(litemallFeedback);
    }

    /**
     * 修改意见反馈
     *
     * @param litemallFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int updateLitemallFeedback(LitemallFeedback litemallFeedback)
    {
        litemallFeedback.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallFeedback(litemallFeedback);
    }

    /**
     * 批量删除意见反馈
     *
     * @param ids 需要删除的意见反馈ID
     * @return 结果
     */
    @Override
    public int deleteLitemallFeedbackByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallFeedbackByIds(ids);
    }

    /**
     * 删除意见反馈信息
     *
     * @param id 意见反馈ID
     * @return 结果
     */
    @Override
    public int deleteLitemallFeedbackById(Long id)
    {
        return baseMapper.deleteLitemallFeedbackById(id);
    }
}
