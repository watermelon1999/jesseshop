package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.common.utils.DateUtils;
import com.jessefyz.module.shop.domain.LitemallComment;
import com.jessefyz.module.shop.mapper.LitemallCommentMapper;
import com.jessefyz.module.shop.service.ILitemallCommentService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 评论Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallCommentServiceImpl
        extends ServiceImpl<LitemallCommentMapper,LitemallComment>
        implements ILitemallCommentService
{

    /**
     * 查询评论
     *
     * @param id 评论ID
     * @return 评论
     */
    @Override
    public LitemallComment selectLitemallCommentById(Long id)
    {
        return baseMapper.selectLitemallCommentById(id);
    }

    /**
     * 查询评论列表
     *
     * @param litemallComment 评论
     * @return 评论
     */
    @Override
    public List<LitemallComment> selectLitemallCommentList(LitemallComment litemallComment)
    {
        return baseMapper.selectLitemallCommentList(litemallComment);
    }

    /**
     * 新增评论
     *
     * @param litemallComment 评论
     * @return 结果
     */
    @Override
    public int insertLitemallComment(LitemallComment litemallComment)
    {
        return this.save(litemallComment)?1:0;
    }

    /**
     * 修改评论
     *
     * @param litemallComment 评论
     * @return 结果
     */
    @Override
    public int updateLitemallComment(LitemallComment litemallComment)
    {
        litemallComment.setUpdateTime(DateUtils.getNowDate());
        return baseMapper.updateLitemallComment(litemallComment);
    }

    /**
     * 批量删除评论
     *
     * @param ids 需要删除的评论ID
     * @return 结果
     */
    @Override
    public int deleteLitemallCommentByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallCommentByIds(ids);
    }

    /**
     * 删除评论信息
     *
     * @param id 评论ID
     * @return 结果
     */
    @Override
    public int deleteLitemallCommentById(Long id)
    {
        return baseMapper.deleteLitemallCommentById(id);
    }
}
