package com.jessefyz.module.shop.domain;

import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 广告对象 litemall_ad
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallAd extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 广告标题 */
    @Excel(name = "广告标题")
    private String name;

    /** 所广告的商品页面或者活动页面链接地址 */
    @Excel(name = "所广告的商品页面或者活动页面链接地址")
    private String link;

    /** 广告宣传图片 */
    @Excel(name = "广告宣传图片")
    private String url;

    /** 广告位置：1则是首页 */
    @Excel(name = "广告位置：1则是首页")
    private Integer position;

    /** 活动内容 */
    @Excel(name = "活动内容")
    private String content;

    /** 广告开始时间 */
    @Excel(name = "广告开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 广告结束时间 */
    @Excel(name = "广告结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 是否启动 */
    @Excel(name = "是否启动")
    private Integer enabled;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("link", getLink())
            .append("url", getUrl())
            .append("position", getPosition())
            .append("content", getContent())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("enabled", getEnabled())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
