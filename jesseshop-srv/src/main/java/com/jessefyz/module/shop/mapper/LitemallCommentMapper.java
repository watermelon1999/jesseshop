package com.jessefyz.module.shop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jessefyz.module.shop.domain.LitemallComment;

import java.util.List;

/**
 * 评论Mapper接口
 *
 * @author ruoyi
 * @date 2020-03-11
 */
public interface LitemallCommentMapper extends BaseMapper<LitemallComment>
{
    /**
     * 查询评论
     *
     * @param id 评论ID
     * @return 评论
     */
    public LitemallComment selectLitemallCommentById(Long id);

    /**
     * 查询评论列表
     *
     * @param litemallComment 评论
     * @return 评论集合
     */
    public List<LitemallComment> selectLitemallCommentList(LitemallComment litemallComment);

    /**
     * 新增评论
     *
     * @param litemallComment 评论
     * @return 结果
     */
    public int insertLitemallComment(LitemallComment litemallComment);

    /**
     * 修改评论
     *
     * @param litemallComment 评论
     * @return 结果
     */
    public int updateLitemallComment(LitemallComment litemallComment);

    /**
     * 删除评论
     *
     * @param id 评论ID
     * @return 结果
     */
    public int deleteLitemallCommentById(Long id);

    /**
     * 批量删除评论
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteLitemallCommentByIds(Long[] ids);
}
