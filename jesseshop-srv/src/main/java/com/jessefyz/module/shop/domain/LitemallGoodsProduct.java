package com.jessefyz.module.shop.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import com.jessefyz.common.mybatis.JsonStringArrayTypeHandler;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 商品货品对象 litemall_goods_product
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
@TableName(autoResultMap = true)
public class LitemallGoodsProduct extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 商品表的商品ID */
    @Excel(name = "商品表的商品ID")
    private Long goodsId;

    /** 商品规格值列表，采用JSON数组格式 */
    @Excel(name = "商品规格值列表，采用JSON数组格式")
    @TableField(typeHandler = JsonStringArrayTypeHandler.class)
    private String[] specifications;

    /** 商品货品价格 */
    @Excel(name = "商品货品价格")
    private BigDecimal price;

    /** 商品货品数量 */
    @Excel(name = "商品货品数量")
    private Integer number;

    /** 商品货品图片 */
    @Excel(name = "商品货品图片")
    private String url;


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("goodsId", getGoodsId())
            .append("specifications", getSpecifications())
            .append("price", getPrice())
            .append("number", getNumber())
            .append("url", getUrl())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
