package com.jessefyz.module.shop.domain;

import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * 类目对象 litemall_category
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
@ToString
@Validated
public class LitemallCategory extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 类目名称 */
    @Excel(name = "类目名称")
    @NotBlank(message = "请输入类目名称")
    private String name;

    /** 类目关键字，以JSON数组格式 */
    @Excel(name = "类目关键字，以JSON数组格式")
    private String keywords;

    /** 类目广告语介绍 */
    @Excel(name = "类目广告语介绍")
    private String description;

    /** 父类目ID */
//    @Excel(name = "父类目ID")
//    private Long pid;

    @Excel(name = "父类目ID")
    private Long parentId;

    /** 类目图标 */
    @Excel(name = "类目图标")
    private String iconUrl;

    /** 类目图片 */
    @Excel(name = "类目图片")
    private String picUrl;

    /** $column.columnComment */
    @Excel(name = "级别")
    private String level;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sortOrder;

}
