package com.jessefyz.module.shop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jessefyz.module.shop.domain.LitemallOrder;
import com.jessefyz.module.shop.domain.LitemallRegion;
import com.jessefyz.module.shop.mapper.LitemallOrderMapper;
import com.jessefyz.module.shop.mapper.LitemallRegionMapper;
import com.jessefyz.module.shop.service.ILitemallRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 行政区域Service业务层处理
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Service
public class LitemallRegionServiceImpl extends ServiceImpl<LitemallRegionMapper, LitemallRegion> implements ILitemallRegionService
{

    /**
     * 查询行政区域
     *
     * @param id 行政区域ID
     * @return 行政区域
     */
    @Override
    public LitemallRegion selectLitemallRegionById(Long id)
    {
        return baseMapper.selectLitemallRegionById(id);
    }

    /**
     * 查询行政区域列表
     *
     * @param litemallRegion 行政区域
     * @return 行政区域
     */
    @Override
    public List<LitemallRegion> selectLitemallRegionList(LitemallRegion litemallRegion)
    {
        return baseMapper.selectLitemallRegionList(litemallRegion);
    }

    /**
     * 新增行政区域
     *
     * @param litemallRegion 行政区域
     * @return 结果
     */
    @Override
    public int insertLitemallRegion(LitemallRegion litemallRegion)
    {
        return baseMapper.insertLitemallRegion(litemallRegion);
    }

    /**
     * 修改行政区域
     *
     * @param litemallRegion 行政区域
     * @return 结果
     */
    @Override
    public int updateLitemallRegion(LitemallRegion litemallRegion)
    {
        return baseMapper.updateLitemallRegion(litemallRegion);
    }

    /**
     * 批量删除行政区域
     *
     * @param ids 需要删除的行政区域ID
     * @return 结果
     */
    @Override
    public int deleteLitemallRegionByIds(Long[] ids)
    {
        return baseMapper.deleteLitemallRegionByIds(ids);
    }

    /**
     * 删除行政区域信息
     *
     * @param id 行政区域ID
     * @return 结果
     */
    @Override
    public int deleteLitemallRegionById(Long id)
    {
        return baseMapper.deleteLitemallRegionById(id);
    }
}
