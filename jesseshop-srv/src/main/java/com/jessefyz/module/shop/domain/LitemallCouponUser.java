package com.jessefyz.module.shop.domain;

import com.jessefyz.common.annotation.Excel;
import com.jessefyz.common.core.domain.BaseEntity;
import com.jessefyz.common.core.domain.BaseShopEntity;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 优惠券用户使用对象 litemall_coupon_user
 *
 * @author ruoyi
 * @date 2020-03-11
 */
@Data
public class LitemallCouponUser extends BaseShopEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 用户ID */
    @Excel(name = "用户ID")
    private Long userId;

    /** 优惠券ID */
    @Excel(name = "优惠券ID")
    private Long couponId;

    /** 使用状态, 如果是0则未使用；如果是1则已使用；如果是2则已过期；如果是3则已经下架； */
    @Excel(name = "使用状态, 如果是0则未使用；如果是1则已使用；如果是2则已过期；如果是3则已经下架；")
    private Integer status;

    /** 使用时间 */
    @Excel(name = "使用时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date usedTime;

    /** 有效期开始时间 */
    @Excel(name = "有效期开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 创建时间 */
    @Excel(name = "有效期结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 订单ID */
    @Excel(name = "订单ID")
    private Long orderId;

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("couponId", getCouponId())
            .append("status", getStatus())
            .append("usedTime", getUsedTime())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("orderId", getOrderId())
            .append("addTime", getAddTime())
            .append("updateTime", getUpdateTime())
            .append("deleted", getDeleted())
            .toString();
    }
}
