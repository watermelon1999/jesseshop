package com.jessefyz.module.shop.req;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 购物车商品货品勾删除请求体
 *
 * @author Jesse
 * @date 2021-09-03
 */
@Data
@ApiModel
@Validated
public class CartDelete
{
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "货品ID列表",required = true)
    @NotEmpty(message = "货品ID不能为空")
    private List<Integer> productIds;

}
